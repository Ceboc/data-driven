# Data Driven

A repository with examples in julia Pluto notebooks with code examples from the book of Steven L. Brunton and J. Nathan Kutz: "Data-driven science and engineering : machine learning, dynamical systems, and control".
