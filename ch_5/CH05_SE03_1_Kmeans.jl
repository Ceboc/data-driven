### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ d1e4062c-cef1-11eb-0ee4-c956437c6975
using Plots, LinearAlgebra, Statistics, Clustering, Distances, StatsPlots

# ╔═╡ ff49dbe0-13e5-4c8f-a3ae-c11063166e18
begin
	# Training and testing set sizes
	n1 = 100 # Train
	n2 = 50 # test
	
	# Random ellipse 1 centered at (0,0)
	x = randn(n1+n2)
	y = 0.5*randn(n1+n2)
	
	# Random ellipse 1 centered at (1,-2)
	x2 = randn(n1+n2) .+ 1
	y2 = 0.2*randn(n1+n2) .-2
	
	# Rotate ellipse 2 by theta
	theta = pi/4
	A = [cos(theta) -sin(theta);
		 sin(theta) cos(theta)]
	x3 = A[1,1]*x2 + A[1,2]*y2
	y3 = A[2,1]*x2 + A[2,2]*y2
end

# ╔═╡ 3d1fac5f-c1f7-4b72-a32e-41371ce0413d
begin
	p1 = scatter(x[1:n1],y[1:n1],c=:red,label=false)
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
end

# ╔═╡ 3f29e9a8-2ba6-4c63-829c-2997d29f1652
begin
	# Training set: first 200 of 240 points
	X1 = hcat(x3[1:n1],y3[1:n1])
	X2 = hcat(x[1:n1],y[1:n1])
	
	Y = vcat(X1,X2)
	Z = hcat(ones(n1),2*ones(n1))
	
	# Test set: remaining 40 points
	x1test = hcat(x3[n1:end],y3[n1:end])
	x2test = hcat(x[n1:end],y[n1:end])
end

# ╔═╡ da100a21-f35d-4bd2-b692-b20d68d85e8e
# Llyod alfgorithm for k-means
begin
	g1 = [-1. 0.]
	g2 = [1. 0.]
	
	gg1 = g1
	gg2 = g2
	for j = 1:5
		class1 = [0. 0.]
		class2 = [0. 0.]#Array{Float64,2}(undef,1,2)
		for jj in 1:length(Y[:,1])
			d1 = norm(g1 .- Y[jj,:])
			d2 = norm(g2 .- Y[jj,:])
			if d1 < d2
				class1= vcat(class1,[Y[jj,1] Y[jj,2]])
			else
				class2= vcat(class2,[Y[jj,1] Y[jj,2]])
			end
		end
		#s1 = size(class1)
		#s2 = size(class2)
		#class1 = class1[2:s1[1],:]
		#class2 = class2[2:s2[1],:]
	
		g1 = [mean(class1[:,1]) mean(class1[:,2])]
		g2 = [mean(class2[:,1]) mean(class2[:,2])]
		
		gg1 = vcat(gg1,g1)
		gg2 = vcat(gg2,g2)
	end
end

# ╔═╡ 6d558dc0-5198-46de-ba36-7f3f956b7c40
begin
	p01 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="a")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[1,1]],[gg1[1,2]],c=:black,label=false,marker=(:star,:green1),)
	scatter!([gg2[1,1]],[gg2[1,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	p02 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="b")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[2,1]],[gg1[2,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[2,1]],[gg2[2,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	p03 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="c")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[3,1]],[gg1[3,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[3,1]],[gg2[3,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	p04 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="d")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[4,1]],[gg1[4,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[4,1]],[gg2[4,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	p05 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="e")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[5,1]],[gg1[5,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[5,1]],[gg2[5,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	p06 = scatter(x[1:n1],y[1:n1],c=:red,label=false,title="f")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	scatter!([gg1[6,1]],[gg1[6,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[6,1]],[gg2[6,2]],c=:black,label=false,marker=(:star,:bisque1))
	
	plot(p01,p02,p03,p04,p05,p06,layout=(3,2))
end

# ╔═╡ ae42695d-4897-42f8-bd3b-54c013d136f5
# kmeans code
begin
	R1 = kmeans(Y',2)
	@assert nclusters(R1) == 2   #No clusters verification
end

# ╔═╡ dd2b4fbc-6c48-4719-b403-00b31491281f
scatter(Y[:,1],Y[:,2])

# ╔═╡ 2cf2c1ad-5546-4259-9c39-2a162b18caac
begin
	scatter(x[1:n1],y[1:n1],c=:red,label=false,title="f")
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	
	scatter!([R1.centers'[1,1]],[R1.centers'[1,2]],label="k1")
	scatter!([R1.centers'[2,1]],[R1.centers'[2,2]],label="k2")
	scatter!([gg1[6,1]],[gg1[6,2]],c=:black,label=false,marker=(:star,:green1))
	scatter!([gg2[6,1]],[gg2[6,2]],c=:black,label=false,marker=(:star,:bisque1))
	
end

# ╔═╡ 69b20213-10c2-4702-bb0e-caf28c9ff842
begin
	c = R1.centers'
	midx = sum(c[:,1])/2
	midy = sum(c[:,2])/2
	slope = (c[2,2]-c[1,2])/(c[2,1]-c[1,1])
	b = midy + (1/slope)*midx
	xsep = -1:0.1:2
	ysep = -(1/slope).*xsep.+b
end

# ╔═╡ 15da0b0f-9585-45ce-b7ef-31aa1a17a0fa
begin
	G1 = scatter(x[1:n1],y[1:n1],c=:red,label=false)
	scatter!(x3[1:n1],y3[1:n1],c=:blue,label=false)
	plot!(xsep,ysep,c=:"black",label=false)
	
	G2 = scatter(x[n1:end],y[n1:end],c=:red,label=false)
	scatter!(x3[n1:end],y3[n1:end],c=:blue,label=false)
	plot!(xsep,ysep,c=:"black",label=false)
	
	plot(G2,G1,layout=(2,1))
end

# ╔═╡ fe38e014-94d5-42b0-96e2-57296b7dfa8f
dist_function(x)  = pairwise(Euclidean(), x, dims = 1)

# ╔═╡ ab2fa5d9-c964-4785-a029-e47e81dfd467
begin
	Y3 = vcat(X1[1:50,:],X2[1:50,:])
	Y2 = dist_function(Y3)
	Z1 = hclust(Y2,linkage=:average,branchorder=:barjoseph)
end

# ╔═╡ 372a17cf-3761-46f9-ab98-2b5c27d395aa
plot(Z1)  #Still no solution to colorize dendograms

# ╔═╡ 9bebb52c-2bd9-4355-a5f2-4530c422da55
begin
	bar(1:100,Z1.order) 
	vline!([50])
	hline!([50])
end

# ╔═╡ Cell order:
# ╠═d1e4062c-cef1-11eb-0ee4-c956437c6975
# ╠═ff49dbe0-13e5-4c8f-a3ae-c11063166e18
# ╠═3d1fac5f-c1f7-4b72-a32e-41371ce0413d
# ╠═3f29e9a8-2ba6-4c63-829c-2997d29f1652
# ╠═da100a21-f35d-4bd2-b692-b20d68d85e8e
# ╠═6d558dc0-5198-46de-ba36-7f3f956b7c40
# ╠═ae42695d-4897-42f8-bd3b-54c013d136f5
# ╠═dd2b4fbc-6c48-4719-b403-00b31491281f
# ╠═2cf2c1ad-5546-4259-9c39-2a162b18caac
# ╠═69b20213-10c2-4702-bb0e-caf28c9ff842
# ╠═15da0b0f-9585-45ce-b7ef-31aa1a17a0fa
# ╠═fe38e014-94d5-42b0-96e2-57296b7dfa8f
# ╠═ab2fa5d9-c964-4785-a029-e47e81dfd467
# ╠═372a17cf-3761-46f9-ab98-2b5c27d395aa
# ╠═9bebb52c-2bd9-4355-a5f2-4530c422da55
