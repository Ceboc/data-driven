### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 035ed978-cd96-11eb-0f8f-e598615dd9b1
begin
	using VegaDatasets, VegaLite, Gadfly, Compose, DataFrames, DataFramesMeta
end

# ╔═╡ 20e7fb6d-53d8-4859-9b96-e56f9f9e97fc
data = DataFrame(dataset("iris"));

# ╔═╡ cf66a8d6-ecbb-435a-aaf4-3e4c91ecf9e0
begin
	p1 = plot(data,x=:sepalLength,y=:sepalWidth,color=:species)
	p2 = plot(data,x=:petalLength,y=:petalWidth,color=:species)
	p3 = plot(data,x=:sepalLength,y=:petalWidth,color=:species)
	p4 = plot(data,x=:petalLength,y=:sepalWidth,color=:species)
	gridstack([p1 p2;p3 p4])
end

# ╔═╡ 2e902004-0468-4978-a881-b3b2194b7114


# ╔═╡ Cell order:
# ╠═035ed978-cd96-11eb-0f8f-e598615dd9b1
# ╠═20e7fb6d-53d8-4859-9b96-e56f9f9e97fc
# ╠═cf66a8d6-ecbb-435a-aaf4-3e4c91ecf9e0
# ╠═2e902004-0468-4978-a881-b3b2194b7114
