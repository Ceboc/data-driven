### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ ec19b078-ceea-11eb-0d19-db7dda7a1d5a
using Plots

# ╔═╡ de1fe7d8-50fc-4df5-bca0-2e341da1cde3
begin
	# Training and test set sizes
	n1 = 100 # Training set size
	n2 = 50 # Test set size
	
	#Random ellipse 1 centered at (-2,0)
	x = randn(n1+n2) .- 2
	y = 0.5*randn(n1+n2)
	
	#Random ellipse 5 centered at (1,0)
	x5 = 2*randn(n1+n2) .+ 2
	y5 = 0.5*randn(n1+n2)
	
	# Random ellipse 2 centered at (2,-2)
	x2 = randn(n1+n2) .+ 2
	y2 = 0.2*randn(n1+n2) .- 2
	
	# Rotate ellipse 2 by theta
	theta = pi/4
	A = zeros(2,2)
	A = [cos(theta) -sin(theta);
		 sin(theta) cos(theta)]
	
	x3 = A[1,1]*x2 + A[1,2]*y2
	y3 = A[2,1]*x2 + A[2,2]*y2
end

# ╔═╡ ff3f3ba3-e413-4ac1-beb5-2b2eb2f8a2ec
begin
	p1 = scatter(x[1:n1],y[1:n1],c=:gray,label=false)
	scatter!(x3[1:n1],y3[1:n1],c=:gray,label=false)
	
	p2 = scatter(x[1:70],y[1:70],c=:gray,label=false)
	scatter!(x3[1:70],y3[1:70],c=:gray,label=false)
	scatter!(x[70:100],y[70:100],c=:green1,label=false)
	scatter!(x3[70:100],y3[70:100],c=:magenta,label=false)
	
	PP = plot(p1,p2,layout=(1,2))
	
	p3 = scatter(x5[1:n1],y5[1:n1],c=:gray,label=false)
	scatter!(x3[1:n1],y3[1:n1],c=:gray,label=false)
	
	p4 = scatter(x5[1:70],y5[1:70],c=:gray,label=false)
	scatter!(x3[1:70],y3[1:70],c=:gray,label=false)
	scatter!(x5[70:100],y5[70:100],c=:green1,label=false)
	scatter!(x3[70:100],y3[70:100],c=:magenta,label=false)
	
	PP2 = plot(p3,p4,layout=(1,2))
	
	plot(PP,PP2,layout=(2,1))
end

# ╔═╡ f5d93d3e-343c-41bd-b227-c5763b37e194
begin
	N1 = 300 
	X1 = 1.5*randn(N1) .- 1.5
	Y1 = 1.2*randn(N1) .+ (X1.+1.5).^2 .- 7
	X2 = 1.5*randn(N1) .+ 1.5
	Y2 = 1.2*randn(N1) .- (X2.-1.5).^2 .+ 7
	
	R = 7 .+ randn(N1)
	th = 2*pi*randn(N1)
	xr = R .* cos.(th)
	yr = R .* sin.(th)
	
	X5 = randn(n1)
	Y5 = randn(n1)
end

# ╔═╡ 9cab4a61-e236-4755-86dd-b21a7bbd00dd
begin
	P1 = scatter(X1,Y1,c=:green1,label=false)
	scatter!(X2,Y2,c=:magenta,label=false)
	
	P2 = scatter(xr,yr,c=:green1,label=false)
	scatter!(X5,Y5,c=:magenta,label=false)
	
	plot(P1,P2,layout=(2,1))
end

# ╔═╡ Cell order:
# ╠═ec19b078-ceea-11eb-0d19-db7dda7a1d5a
# ╠═de1fe7d8-50fc-4df5-bca0-2e341da1cde3
# ╠═ff3f3ba3-e413-4ac1-beb5-2b2eb2f8a2ec
# ╠═f5d93d3e-343c-41bd-b227-c5763b37e194
# ╠═9cab4a61-e236-4755-86dd-b21a7bbd00dd
