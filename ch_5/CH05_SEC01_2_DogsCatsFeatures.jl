### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 5a01b6a0-ce46-11eb-034e-79c23d68c44c
using MAT, Images, LinearAlgebra, StatsBase, StatPlots

# ╔═╡ 2b4eba89-3f23-43f9-a51b-1eb04f183460
begin
	dogs_file = matopen("../DATA/dogData.mat")
	dogs_vars = read(dogs_file)
	close(dogs_file)
	
	cats_file = matopen("../DATA/catData.mat")
	cats_vars = read(cats_file)
	close(cats_file)
end

# ╔═╡ 0a061124-c4bb-4652-9f28-a1b4ac6edfcd
begin
	dogs_file_w = matopen("../DATA/dogData_w.mat")
	dogs_vars_w = read(dogs_file_w)
	close(dogs_file_w)
	
	cats_file_w = matopen("../DATA/catData_w.mat")
	cats_vars_w = read(cats_file_w)
	close(cats_file_w)
end

# ╔═╡ 670fe96a-6566-4926-97fb-e210e1e3a7aa
begin
	dog = Int64.(dogs_vars["dog"])
	dog_wave = Int64.(dogs_vars_w["dog_wave"])
	
	cat = Int64.(cats_vars["cat"])
	cat_wave = Int64.(cats_vars_w["cat_wave"])
end

# ╔═╡ 37363de3-b4fc-4702-996a-042c92fc219f
begin
	CD = hcat(dog,cat)
	CD2= hcat(dog_wave,cat_wave)
end

# ╔═╡ 93fc612e-5159-4c61-b6e3-8a1680cc53f1
begin
	u, s, vt = svd(CD .- mean(CD))
	u2, s2, vt2 = svd(CD2 .- mean(CD2))
end

# ╔═╡ aa85fec7-e112-411f-a0fa-a2d32930fa4e
begin
	f1 = reverse(reshape(u[:,1],(64,64)),dims=2)
	f2 = reverse(reshape(u[:,2],(64,64)),dims=2)
	f3 = reverse(reshape(u[:,3],(64,64)),dims=2)
	f4 = reverse(reshape(u[:,4],(64,64)),dims=2)
end

# ╔═╡ 0bd44467-e3ec-429d-8e55-c1f741d64521
mosaicview(Gray.(f1./maximum(f1)),Gray.(f2./maximum(f2)),Gray.(f3./maximum(f3)),Gray.(f4./maximum(f4)),nrow=2,ncol=2,npad=10)

# ╔═╡ 1a4eb63d-ce71-436e-b465-90ac72d53ee5
begin
	b1 = bar(1:length(s),vt'[1,:],label=false)
	b2 = bar(1:length(s),vt'[2,:],label=false)
	b3 = bar(1:length(s),vt'[3,:],label=false)
	b4 = bar(1:length(s),vt'[4,:],label=false)
	
	plot(b1,b2,b3,b4,layout=(4,1))
end

# ╔═╡ 6ded8144-f183-4151-b584-a1e520f18130
begin
	h1 = reverse(reshape(dog_wave[:,1],(32,32)),dims=2)
	h2 = reverse(reshape(dog_wave[:,2],(32,32)),dims=2)
	h3 = reverse(reshape(dog_wave[:,3],(32,32)),dims=2)
	h4 = reverse(reshape(dog_wave[:,4],(32,32)),dims=2)
	
	mosaicview(Gray.(h1./maximum(h1)),Gray.(h2./maximum(h2)),Gray.(h3./maximum(h3)),Gray.(h4./maximum(h4)),nrow=2,ncol=2,npad=10)
end

# ╔═╡ af9ff632-02ae-4c98-9543-e6eab703695b
begin
	g1 = reverse(reshape(u2[:,1],(32,32)),dims=2)
	g2 = reverse(reshape(u2[:,2],(32,32)),dims=2)
	g3 = reverse(reshape(u2[:,3],(32,32)),dims=2)
	g4 = reverse(reshape(u2[:,4],(32,32)),dims=2)
	
	mosaicview(Gray.(g1./maximum(g1)),Gray.(g2./maximum(g2)),Gray.(g3./maximum(g3)),Gray.(g4./maximum(g4)),nrow=2,ncol=2,npad=10)
end

# ╔═╡ fee8da85-2657-42f3-9a38-3b6883b98db8
begin
	bb1 = bar(1:length(s2),vt2'[1,:],label=false)
	bb2 = bar(1:length(s2),vt2'[2,:],label=false)
	bb3 = bar(1:length(s2),vt2'[3,:],label=false)
	bb4 = bar(1:length(s2),vt2'[4,:],label=false)
	
	plot(bb1,bb2,bb3,bb4,layout=(4,1))
end

# ╔═╡ 00c6f043-fcc6-4499-ad81-ff0b0f6d09ad
begin
	xbin = collect(range(-0.25,0.25,length=25))
	xbin_edges=vcat(xbin,-xbin[1]+(xbin[2]-xbin[1])) .- (xbin[2]-xbin[1])/2
	
	H1 = fit(Histogram,vt'[1,1:80],xbin_edges)
	H1p = fit(Histogram,vt'[1,80:end],xbin_edges)
	H2 = fit(Histogram,vt'[2,1:80],xbin_edges)
	H2p = fit(Histogram,vt'[2,80:end],xbin_edges)
	H3 = fit(Histogram,vt'[3,1:80],xbin_edges)
	H3p = fit(Histogram,vt'[3,80:end],xbin_edges)
	H4 = fit(Histogram,vt'[4,1:80],xbin_edges)
	H4p = fit(Histogram,vt'[4,80:end],xbin_edges)
	
	HH1 = fit(Histogram,vt2'[1,1:80],xbin_edges)
	HH1p = fit(Histogram,vt2'[1,80:end],xbin_edges)
	HH2 = fit(Histogram,vt2'[2,1:80],xbin_edges)
	HH2p = fit(Histogram,vt2'[2,80:end],xbin_edges)
	HH3 = fit(Histogram,vt2'[3,1:80],xbin_edges)
	HH3p = fit(Histogram,vt2'[3,80:end],xbin_edges)
	HH4 = fit(Histogram,vt2'[4,1:80],xbin_edges)
	HH4p = fit(Histogram,vt2'[4,80:end],xbin_edges)
end

# ╔═╡ 12e8f929-6115-4852-b40a-641fd90ee175
begin
	p1 = plot(xbin,H1.weights,label="dogs")
	p1p= plot!(xbin,H1p.weights,label="cats")
	
	p2 = plot(xbin,H2.weights,label=false)
	p2p= plot!(xbin,H2p.weights,label=false)
	
	p3 = plot(xbin,H3.weights,label=false)
	p3p= plot!(xbin,H3p.weights,label=false)
	
	p4 = plot(xbin,H4.weights,label=false)
	p4p= plot!(xbin,H4p.weights,label=false)
	
	p_raw = plot(p1,p2,p3,p4,layout=(4,1))
	
	pp1 = plot(xbin,HH1.weights,label="dogs")
	pp1p= plot!(xbin,HH1p.weights,label="cats")
	
	pp2 = plot(xbin,HH2.weights,label=false)
	pp2p= plot!(xbin,HH2p.weights,label=false)
	
	pp3 = plot(xbin,HH3.weights,label=false)
	pp3p= plot!(xbin,HH3p.weights,label=false)
	
	pp4 = plot(xbin,HH4.weights,label=false)
	pp4p= plot!(xbin,HH4p.weights,label=false)
	
	p_wav = plot(pp1,pp2,pp3,pp4,layout=(4,1))
	
	plot(p_raw,p_wav,layout=(1,2))
end

# ╔═╡ 55dc8d07-1ced-44a1-9dc5-d87bd62e9c9c
begin
	sc1 = scatter(vt'[1,1:80],vt'[2,1:80],vt'[3,1:80],label="dogs",camera=(45,60))
	scatter!(vt'[1,80:end],vt'[2,80:end],vt'[3,80:end],label="cats")
	
	sc2 = scatter(vt2'[1,1:80],vt2'[2,1:80],vt2'[3,1:80],label=false,camera=(45,60))
	scatter!(vt2'[1,80:end],vt2'[2,80:end],vt2'[3,80:end],label=false)
	
	plot(sc1,sc2,layout=(2,1),size=(400,800))
end

# ╔═╡ Cell order:
# ╠═5a01b6a0-ce46-11eb-034e-79c23d68c44c
# ╠═2b4eba89-3f23-43f9-a51b-1eb04f183460
# ╠═0a061124-c4bb-4652-9f28-a1b4ac6edfcd
# ╠═670fe96a-6566-4926-97fb-e210e1e3a7aa
# ╠═37363de3-b4fc-4702-996a-042c92fc219f
# ╠═93fc612e-5159-4c61-b6e3-8a1680cc53f1
# ╠═aa85fec7-e112-411f-a0fa-a2d32930fa4e
# ╠═0bd44467-e3ec-429d-8e55-c1f741d64521
# ╠═1a4eb63d-ce71-436e-b465-90ac72d53ee5
# ╠═6ded8144-f183-4151-b584-a1e520f18130
# ╠═af9ff632-02ae-4c98-9543-e6eab703695b
# ╠═fee8da85-2657-42f3-9a38-3b6883b98db8
# ╠═00c6f043-fcc6-4499-ad81-ff0b0f6d09ad
# ╠═12e8f929-6115-4852-b40a-641fd90ee175
# ╠═55dc8d07-1ced-44a1-9dc5-d87bd62e9c9c
