### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ cc94a958-d381-11eb-335a-718425080349
using MAT, LinearAlgebra, Distributions, Plots, ScikitLearn, PyCall, Statistics

# ╔═╡ 5fa85ff1-2e78-487e-9ba4-c168e400cda1
using GaussianMixtures: GMM

# ╔═╡ a2cf4cd8-1a85-495d-9da4-006483c222a5
logrange(x1, x2, n) = (10^y for y in range(log10(x1), log10(x2), length=n))

# ╔═╡ 01d4bbe8-8cdb-4af4-b0a4-e219bc7807d0
begin
	dogs = read(matopen("../DATA/dogData_w.mat"),"dog_wave")
	cats = read(matopen("../DATA/catData_w.mat"),"cat_wave")
	
	CD = hcat(dogs,cats)
end

# ╔═╡ de155829-7f2f-41c9-8981-691162814f6f
begin
	u, s, v = svd(CD.-mean(CD));
	v = v
end

# ╔═╡ 067e8bb3-39af-4cb0-8f7b-9000b70ac32b
begin
	scatter(v[1:80,2],v[1:80,4])
	scatter!(v[81:end,2],v[81:end,4])
end

# ╔═╡ 9d75f521-ff8e-43a2-b06d-1a8a6c84d434
dogcat = v[:,2:2:4]

# ╔═╡ 4542de43-088f-4f35-ac64-69e1dd49770b
begin
	# fit a Gaussian Mixture Model with two components
	clf = GMM(n_components=2, kind=:full)

	fit!(clf, dogcat)
end

# ╔═╡ 073af0b9-a90a-4c2e-bff9-90cd25a8e240
begin
	x = range(-0.3,0.3,length=100)
	y = range(-0.3,0.3,length=100)
	
	X = [x for x in x, y in y]
	Y = [y for x in x, y in y]
	
	XX = hcat(X[:],Y[:])
	
	Z = -score_samples(clf,XX)
	
	Z = reshape(Z,size(X))
end

# ╔═╡ 53a03902-22e4-4a70-b5c8-391f66b4e9bd
begin
	contour(x, y, log10.(abs.(Z)))
	scatter!(v[1:80,2],v[1:80,4],label="Dogs")
	scatter!(v[81:end,2],v[81:end,4],label ="Cats" )
end

# ╔═╡ Cell order:
# ╠═cc94a958-d381-11eb-335a-718425080349
# ╠═5fa85ff1-2e78-487e-9ba4-c168e400cda1
# ╠═a2cf4cd8-1a85-495d-9da4-006483c222a5
# ╠═01d4bbe8-8cdb-4af4-b0a4-e219bc7807d0
# ╠═de155829-7f2f-41c9-8981-691162814f6f
# ╠═067e8bb3-39af-4cb0-8f7b-9000b70ac32b
# ╠═9d75f521-ff8e-43a2-b06d-1a8a6c84d434
# ╠═4542de43-088f-4f35-ac64-69e1dd49770b
# ╠═073af0b9-a90a-4c2e-bff9-90cd25a8e240
# ╠═53a03902-22e4-4a70-b5c8-391f66b4e9bd
