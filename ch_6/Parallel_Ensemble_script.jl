using Plots: Statistics, length
using Base: Float64
using Distributed
using Plots

addprocs()
@everywhere using Pkg
@everywhere Pkg.activate(".")
@everywhere using DifferentialEquations



# Linear ODE which starts at 0.5 and solves from t=0.0 to t=1.0
prob = ODEProblem((u,p,t)->1.01u,0.5,(0.0,1.0))

@everywhere function prob_func(prob,i,repeat)
    remake(prob,u0=rand()*prob.u0)
end

  ensemble_prob = EnsembleProblem(prob,prob_func=prob_func)
  sim = solve(ensemble_prob,Tsit5(),EnsembleDistributed(),trajectories=100)

plotly()
plot(sim,linealpha=0.4)

# Stochastic Differential Equation 

# Lotka-Volterra system
function f(du,u,p,t)
  du[1] = p[1] * u[1] -p[2] * u[1] * u[2]
  du[2] = -3 * u[2] + u[1] * u[2]
end

# Multiplicative noise
function g(du,u,p,t)
  du[1] = p[3] * u[1]
  du[2] = p[4] * u[2]
end

# Build of SDE
p = [1.5,1.0,0.1,0.1]
prob = SDEProblem(f,g,[1.0,1.0],(0.0,10.0),p)

function prob_func(prob,i,repeat)
  x = 0.3rand(2)
  remake(prob,p=[p[1:2];x])
end

# Declare ensemble 
ensemble_prob = EnsembleProblem(prob,prob_func=prob_func)
sim = solve(ensemble_prob,SRIW1(),trajectories=10)

plot(sim,linealpha=0.6,color=:blue,vars=(0,1),title="Phase Space Plot")
plot!(sim,linealpha=0.6,color=:red,vars=(0,2),title="Phase Space Plot")

# Ensemble summary
summ = EnsembleSummary(sim,0:0.1:10)

pyplot()
plot(summ,fillalpha=0.5,title="Summary with Quantile Bounds")

## Using the Reduction to Halt When Estimator is Within Tolerance
@everywhere using Statistics


function output_func(sol,i)
  last(sol)
end

# Linear ODE which start at 0.5 and solves from t = 0.0 to t = 1.0
prob = ODEProblem((u,p,t)->1.01u,0.5,(0.0,1.0))

function prob_func(prob,i,repeat)
  remake(prob,u0=rand()*prob.u0)
end

function reduction(u,batch,I)
  u = append!(u,batch)
  finished = (var(u) / sqrt(last(I))) / mean(u) < 0.5
  u, finished
end

prob2 = EnsembleProblem(prob,prob_func=prob_func,output_func=output_func,reduction=reduction,u_init=Vector{Float64}())
sim = solve(prob2, Tsit5(),trajectories=10_000,batch_size=20)

#function reduction(u,batch,I)
#  u+sum(batch),false
#end
#prob2 = EnsembleProblem(prob,prob_func=prob_func,output_func=output_func,reduction=reduction,u_init=0.0)
#sim2 = solve(prob2,Tsit5(),trajectories=100,batch_size=20)


# Using the Analysis Tools

function f(du,u,p,t)
  for i = 1:length(u)
    du[i] = 1.01*u[i]
  end
end

function σ(du,u,p,t)
  for i in 1:length(u)
    du[i] = 0.87*u[i]
  end
end

prob = SDEProblem(f,σ,ones(4,2)/2,(0.0,1.0))

prob2 = EnsembleProblem(prob)

sim = solve(prob2,SRIW1(),dt=1//2^(3),trajectories=10,adaptative=false)

using DifferentialEquations.EnsembleAnalysis

m,v = timestep_meanvar(sim,3)

m,v = timepoint_meanvar(sim,0.5)

m_series,v_series = timeseries_steps_meanvar(sim)

ts = 0:0.1:1
m_series = timeseries_point_mean(sim,ts)

timeseries_steps_meancov(sim) # Use the time steps, assume fixed dt
timeseries_point_meancov(sim,0:1//2^(3):1,0:1//2^(3):1) 

summ = EnsembleSummary(sim)

summ = EnsembleSummary(sim,0.0:0.1:1.0)

plot(summ;idxs=3)

plot(summ;idxs=(3,5),error_style=:bars)

plot(summ;error_style=:none)

