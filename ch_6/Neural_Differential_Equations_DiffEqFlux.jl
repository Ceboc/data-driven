using DifferentialEquations
using Plots

function lotka_volterra(du,u,p,t)
    x, y = u
    α, β, δ, γ = p
    du[1] = dx = α*x - β*x*y
    du[2] = dy = -δ*y + γ*x*y
end

u0 = [1.0,1.0]
tspan = (0.0,10.0)
p = [1.5,1.0,3.0,1.0]
prob = ODEProblem(lotka_volterra,u0,tspan,p)

sol = solve(prob)

plot(sol)

sol = solve(prob,Tsit5(),saveat=0.1)

A = sol[1,:]

plot(sol)
t = 0:0.1:10.0
scatter!(t,A)

using Flux, DiffEqFlux
p = [2.2,1.0,2.0,0.4]
params = Flux.params(p)

function predict_rd() #Our 1-layer "neural network"
    solve(prob,Tsit5(),p=p,saveat=0.1)[1,:] #override with new parameters
end

loss_rd() = sum(abs2,x-1 for x in predict_rd()) # loss function

data = Iterators.repeated((),100)

opt = ADAM(0.1)

cb = function () # Callbacj function to observe training
    display(loss_rd())
    # using `remake` to re-create our `prob` with current parameters `p`
    display(plot(solve(remake(prob,p=p),Tsit5(),saveat=0.1),ylim=(0,6)))
end

# Display the ODE with the initila parameter values
cb()

Flux.train!(loss_rd, params,data, opt, cb=cb)

rober = @ode_def Rober begin
    dy₁ = -k₁*y₁+k₃*y₂*y₃
    dy₂ =  k₁*y₁-k₂*y₂^2-k₃*y₂*y₃
    dy₃ =  k₂*y₂^2
end k₁ k₂ k₃

prob = ODEProblem(rober,[1.0;0.0;0.0],(0.0,1e11),(0.04,3e7,1e4))
using Sundials # to load CVODE_Adams
solve(prob,CVODE_Adams())

sol = solve(prob,KenCarp4())
plot(sol,xscale=:log10,tspan=(0.1,1e11))


# Delay Differential DifferentialEquations
function delay_lotka_volterra(du,u,h,p,t)
    x, y = u
    α, β, δ, γ = p
    du[1] = dx = (α - β*y)*h(p,t-0.1)[1]
    du[2] = dy = (δ*x - γ)*y
end

h(p,t) = ones(eltype(p),2)
prob = DDEProblem(delay_lotka_volterra,[1.0,1.0],h,(0.0,10.0),constant_lags=[0.1])

p = [2.2,1.0,2.0,0.4]
params = Flux.params(p)

using DiffEqSensitivity

function predict_rd_dde()
    solve(prob,MethodOfSteps(Tsit5()),p=p,sensealg=TrackerAdjoint(),saveat=0.1)[1,:]
end

loss_rd_dde() = sum(abs2,x-1 for x in predict_rd_dde())
loss_rd_dde()



function lotka_volterra_noise(du,u,p,t)
    du[1] = 0.1u[1]
    du[2] = 0.1u[2]
end
prob = SDEProblem(lotka_volterra,lotka_volterra_noise,[1.0,1.0],(0.0,5.0))
  
p = [2.2, 1.0, 2.0, 0.4]
params = Flux.params(p)
function predict_sde()
    solve(prob,SOSRI(),p=p,sensealg=TrackerAdjoint(),saveat=0.1,
                       abstol=1e-1,reltol=1e-1)[1,:]
end
loss_rd_sde() = sum(abs2,x-1 for x in predict_sde())
loss_rd_sde()

data = Iterators.repeated((), 100)
opt = ADAM(0.1)
cb = function ()
  display(loss_rd_sde())
  display(plot(solve(remake(prob,p=p),SOSRI(),saveat=0.1),ylim=(0,6)))
end

# Display the ODE with the current parameter values.
cb()

Flux.train!(loss_rd_sde, params, data, opt, cb = cb)


# Implementing the Neural ODE layer in Julia

dudt = Chain(Dense(2,50,tanh),Dense(50,2))

tspan = (0.0f0,25.0f0)
node = NeuralODE(dudt,tspan,Tsit5(),saveat=0.1)

u0 = Float32[2.; 0.]
datasize = 30
tspan = (0.0f0,1.5f0)

function trueODEfunc(du,u,p,t)
    true_A = [-0.1 2.0; -2.0 -0.1]
    du .= ((u.^3)'true_A)'
end
t = range(tspan[1],tspan[2],length=datasize)
prob = ODEProblem(trueODEfunc,u0,tspan)
ode_data = Array(solve(prob,Tsit5(),saveat=t))

dudt = Chain(x -> x.^3,
             Dense(2,50,tanh),
             Dense(50,2))
n_ode = NeuralODE(dudt,tspan,Tsit5(),saveat=t,reltol=1e-7,abstol=1e-9)
ps = Flux.params(n_ode)

pred = n_ode(u0) # Get the prediction using the correct initial condition
scatter(t,ode_data[1,:],label="data")
scatter!(t,pred[1,:],label="prediction")

function predict_n_ode()
    n_ode(u0)
  end
  loss_n_ode() = sum(abs2,ode_data .- predict_n_ode())

  data = Iterators.repeated((), 1000)
opt = ADAM(0.1)
cb = function () #callback function to observe training
  display(loss_n_ode())
  # plot current prediction against data
  cur_pred = predict_n_ode()
  pl = scatter(t,ode_data[1,:],label="data")
  scatter!(pl,t,cur_pred[1,:],label="prediction")
  display(plot(pl))
end

# Display the ODE with the initial parameter values.
cb()

Flux.train!(loss_n_ode, ps, data, opt, cb = cb)
