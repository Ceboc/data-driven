### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ c5d23e34-e07c-11eb-326e-2b686ca192ea
using Flux

# ╔═╡ 1f8df7e1-68a8-4e75-9a83-c0930e7fbe8a
md"
This notebook take code and notes from Flux.jl documentation [Flux Basics](https://fluxml.ai/Flux.jl/stable/models/basics/)
# Flux Basics

## Taking Gradients

Flux's core feature is taking gradients of Julia code. The `gradient` function takes another Julia function `f` an a set of arguments, and returns the gradient with respect to each argument.

"

# ╔═╡ e9bf3ef8-a158-45eb-8190-c995a4bfad09
f(x) = 3x^2 + 2x + 1;

# ╔═╡ 124e9daa-1bfb-465e-a424-1f63cac4ba2b
md"When a function has many parameters, we can get gradients of each one at the same time"

# ╔═╡ 5af9940c-7d12-4f5f-bc9f-35fd214895cd
f(x,y) = sum((x .- y).^2);

# ╔═╡ 57dfea0d-7e20-4dde-a900-61f48193ba3d
df(x) = gradient(f,x)[1]; #df/dx = 6x + 2

# ╔═╡ 6e788316-45d8-4ad4-ada3-7937acb47e39
df(2)

# ╔═╡ c820c798-1c45-436a-8ded-436524564165
d2f(x) = gradient(df,x)[1]; #d²f/dx² = 6

# ╔═╡ 69fdab7d-1cc0-47d5-a822-f621ba2951e7
d2f(2)

# ╔═╡ 8b73c8fb-da45-48d6-a13a-8d01618cfff0
gradient(f, [2,1],[2,0])

# ╔═╡ 8172cd55-0a6c-4e52-923f-451f041f51c0
md"These gradients are based on `x` and `y`. Flux by instead taking gradients based on the weights and biases that make up the parameters of a model.

Machine learning often can have hundreds of parameters, so Flux lets you work with collections of parameters, via de `params` functions. You can get the gradient of all parameters used in a program without explicitly passing them in.
"

# ╔═╡ 9ffde980-cf5c-4eea-a4d8-902015ec9870
x = [2,1];

# ╔═╡ 9b54c388-3813-40f7-b4de-5bb001f5ebf9
y = [2,0];

# ╔═╡ 5f037ad7-e800-4ea8-842a-dc920c432d39
gs = gradient(params(x,y)) do
		f(x,y)
end

# ╔═╡ bcb90d1f-c3a2-4e6d-9c1f-d297232664fc
gs[x]

# ╔═╡ f200dafb-6e9c-4c7d-93a1-f4ecf08f7222
gs[y]

# ╔═╡ cdce3049-7c3c-4c5b-931c-077579073a67
md"Here, `gradient` takes a zero-argument function; no arguments are necessary because the `params` tell it what to differentiate.

This will come in really handy when dealing with big, complicated models. For now, though, let's start with something simple."

# ╔═╡ dae9b292-e301-4472-b8be-75add4b162b3
md"## Building Simple Models
Consider a simple linear regression, which tries to predict an output array `y` from an imput `x`."

# ╔═╡ 67862dba-616b-4d48-936d-f7bbcab8174e
W = rand(2,5)

# ╔═╡ 004b49f3-918d-4eeb-9ec5-2e19df59458a
b = rand(2)

# ╔═╡ b53fae79-8b2f-432b-a735-784d64b74a2e
predict(x) = W*x .+ b;

# ╔═╡ 587c6dc8-16d5-412d-bd91-4bff307ad3df
function loss(x,y)
	ŷ = predict(x)
	sum((y .- ŷ).^2)
end;

# ╔═╡ e3909b31-6750-44d5-9d5c-66aa90024504
x_m, y_m = rand(5), rand(2)

# ╔═╡ 2896c3d1-a6f4-45f0-b8f2-9f13b999a0a4
loss(x_m,y_m)

# ╔═╡ c89dc98e-3b70-4ecc-ba57-7c763c8fe44d
md"To improve the prediction we can take the gradients of the loss with respect to `W` and `b` and perform gradeint descent."

# ╔═╡ 12e24b6b-9e73-4d23-85ff-82a416eea4ad
gs_m = gradient(() -> loss(x_m, y_m), params(W, b))

# ╔═╡ a4a56238-2fe8-4e15-a9b5-886860eb167f
md"Now that we have gradients, we can pull them out and update `W` to train the model"

# ╔═╡ 90c4c99b-9b14-4de3-a6b6-0b7f9fd64b26
W⁻ = gs_m[W]

# ╔═╡ c70a3712-2064-4d9c-b617-a11913cca2c1
W .-= 0.1 .* W⁻

# ╔═╡ b0b3d82c-5e13-46de-b5ae-97914d14c6ff
loss(x_m,y_m)

# ╔═╡ d5fc3d65-b8a7-490c-b264-f9c437efb029
md"The loss has decreased a little, meaning that our prediction `x` is closer to the target `y`, If we have some data we can already try training the model.

All deep learning in Flux, however complex, is a simple generalisation of this example. Of course, models can look very different - they might have millions of parameters or complex control flow. Let's see how Flux handles more complex models.

## Building Layers

It's common to create more complex model than the linear regression above. For example, we might want to have two linear layers with a nonlinearity like [sigmoid](https://en.wikipedia.org/wiki/Sigmoid_function)(`σ`) in between them. In te above style we could write this as:"

# ╔═╡ f3a27060-19cd-4001-ab32-883cdbb6d922
W1 = rand(3,5)

# ╔═╡ d07adeeb-4808-4bd7-9bc0-c30a9fdc4b32
b1 = rand(3)

# ╔═╡ 15ef958d-38f7-4222-816c-6ab6c885c301
layer1(x) = W1 * x .+ b1;

# ╔═╡ 72afaa8b-ca91-4196-af7c-be0b9be64267
W2 = rand(2,3)

# ╔═╡ e192df9d-96f2-404e-9ad6-f7739400d336
b2 = rand(2)

# ╔═╡ a56b9c3a-7359-4574-a098-c77c374b58a3
layer2(x) = W2 * x .+ b2;

# ╔═╡ 4c11e7de-1a77-4783-8e95-03160caa0d6f
model1(x) = layer2(σ.(layer1(x)))

# ╔═╡ 41637e1c-a9aa-480f-bdca-b98d76935965
md"This works but is fairly unwieldy, with a lot of repetition - especially as we add more layers. One way to factor this out is to create a function that return alinear layers."

# ╔═╡ 321811df-c112-496e-a012-70b0e064c2f0
function linear(in, out)
	W = randn(out,in)
	b = randn(out)
	x -> W*x .+ b
end;

# ╔═╡ 35e0da79-c6d3-4bb0-8687-ed33e00dcbcb
linear1 = linear(5,3) # we can acces linear1.W etc

# ╔═╡ 0e2dbdfb-98c9-4ca3-adc3-39fb03e12663
linear2 = linear(3,2)

# ╔═╡ bb50869e-3a13-484b-ace0-cddaa1bafa77
model(x) = linear2(σ.(linear1(x)));

# ╔═╡ ba7a424b-cfd5-4d4d-8832-92d0bb96215c
model(rand(5))  # => 2-element vector

# ╔═╡ a0d13bf2-2eea-438c-ba0e-c276116ec3b5
md"Another (equivalent) way is to create a struct that explicitly represents the affine layer."

# ╔═╡ bb60013d-d2be-4cfc-a0da-3675460aec11
begin
	struct Affine
		W
		b
	end

	Affine(in::Integer, out::Integer) = 
		Affine(randn(out,in), randn(out))
	
	# Overload call, so the object can be used as a function
	(m::Affine)(x) = m.W * x .+ m.b
end

# ╔═╡ 1dbb0bc5-ecd1-4d75-a31d-f842e3a197dc
a = Affine(10,5)

# ╔═╡ 0bce1169-3bae-4381-bccc-2d50668c3d69
a(rand(10)) # => 5-element vector

# ╔═╡ 2899b823-2d57-481d-99b8-0bdb6e57e763
md"Congratulations! You just build the `Dense` layer that comes with Flux. Flux has manu interesting layers available, but they're all things you coul have build yourself very easily.

(There is one small difference with `Dense` - for convenience it also taken an activation function, like `Dense(10,5,σ)`.)"

# ╔═╡ a6671a2b-67ae-4945-96c4-e0b97e9358ed
md"## Stacking It Up
It's pretty common to write models that look something like"

# ╔═╡ a58960d8-11f3-43e6-937a-b55acdb1fa0a
begin
	layer1_n = Dense(10,5,σ)
	layer2_n = Dense(4,10,σ)
	layer3_n = Dense(1,4,σ)
end

# ╔═╡ 970dbcce-aec1-4a2b-9ea0-cdb2433326af
model_n(x) = layer3(layer2(layer1_n(x)))

# ╔═╡ dd767b4f-617a-42f4-b470-9b19214ab494
md"For long chains, it might be a bit more intuitive to have a list of layers, lije this"

# ╔═╡ 81e609e5-2efd-40a2-ba34-7b56c6c1b9bc
layers = [Dense(10,5,σ),Dense(5,2),softmax]

# ╔═╡ 8091c7d0-36b6-4e35-a912-09a4543e896f
model_o(x) = foldl((x,m) -> m(x), layers, init = x)

# ╔═╡ 0f29e2db-0d6e-4ad0-bfcd-025c049916a7
model_o(rand(10)) # => 2-element vector

# ╔═╡ 97da2c64-3692-444d-8a48-7cad859db55b
md"Handily, this is also provided for in Flux:"

# ╔═╡ f418d0ae-0520-40e4-97f1-9acea12a2653
model2 = Chain(
	Dense(10, 5, σ),
	Dense(5,2),
	softmax
	);

# ╔═╡ 9e01ef32-60fc-456f-8a0c-5927642d35b3
model2(rand(10))

# ╔═╡ 364a8ecd-df4a-4fb3-b4f7-6f9d4cdf7ed6
md"This quickly start to look like a high-level deep learning library; yet you can see how it falls out simple abstractions, and we lose none of the power of Julia code.

A nice property of this approach is that because ''models'' are just functions (possibly with trainable parameters), you can also this as simple function composition."

# ╔═╡ 74407477-dde0-4145-b407-06e554eb8f56
m = Dense(5,2) ∘ Dense(10, 5, σ)

# ╔═╡ 35254aa0-437c-4e7c-829b-66e22e29e1d1
m(rand(10))

# ╔═╡ 14ed535d-ccff-41c6-a711-df2b8f37037d
md"Likewise, `Chain` will happily work with any Julia function."

# ╔═╡ 54e261ee-1f25-4d38-be76-85042837ca75
mm = Chain(x -> x^2, x -> x+1)

# ╔═╡ b79ff58c-ae91-4698-bbd5-4df306832c9e
mm(5) # => 26

# ╔═╡ 2b99e31a-f68d-49db-96a2-9b8201b79db2
md"## Layer helpers
Flux provides a set of helpers for custom layers, wich you can enable by calling"

# ╔═╡ 8f953e98-8d6c-4ee0-89a8-d4edda668bd5
#Flux.@functor Affine

# ╔═╡ 909acd1e-5890-4194-87ae-8d46882c2304
md"This enables a useful extra set of functionality for our `Affine` layer, such as [collecting its parameters](https://fluxml.ai/Flux.jl/stable/training/optimisers/) or [moving it to the GPU](https://fluxml.ai/Flux.jl/stable/gpu/)."

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Flux = "587475ba-b771-5e3f-ad9e-33799f191a9c"

[compat]
Flux = "~0.12.4"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[AbstractFFTs]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "485ee0867925449198280d4af84bdb46a2a404d0"
uuid = "621f4979-c628-5d54-868e-fcf4e3e8185c"
version = "1.0.1"

[[AbstractTrees]]
git-tree-sha1 = "03e0550477d86222521d254b741d470ba17ea0b5"
uuid = "1520ce14-60c1-5f80-bbc7-55ef81b5835c"
version = "0.3.4"

[[Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "84918055d15b3114ede17ac6a7182f68870c16f7"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.1"

[[ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[BFloat16s]]
deps = ["LinearAlgebra", "Test"]
git-tree-sha1 = "4af69e205efc343068dc8722b8dfec1ade89254a"
uuid = "ab4f0b2a-ad5b-11e8-123f-65d77653426b"
version = "0.1.0"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[CEnum]]
git-tree-sha1 = "215a9aa4a1f23fbd05b92769fdd62559488d70e9"
uuid = "fa961155-64e5-5f13-b03f-caf6b980ea82"
version = "0.4.1"

[[CUDA]]
deps = ["AbstractFFTs", "Adapt", "BFloat16s", "CEnum", "CompilerSupportLibraries_jll", "DataStructures", "ExprTools", "GPUArrays", "GPUCompiler", "LLVM", "LazyArtifacts", "Libdl", "LinearAlgebra", "Logging", "Printf", "Random", "Random123", "RandomNumbers", "Reexport", "Requires", "SparseArrays", "SpecialFunctions", "TimerOutputs"]
git-tree-sha1 = "3e5a6cf59289a60202a0f63fdded29473544d553"
uuid = "052768ef-5323-5732-b1bb-66c8b64840ba"
version = "3.3.2"

[[ChainRules]]
deps = ["ChainRulesCore", "Compat", "LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "85c579fa131b5545eef874a5b413bb3b783e21c6"
uuid = "082447d4-558c-5d27-93f4-14fc19e9eca2"
version = "0.8.21"

[[ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "dcc25ff085cf548bc8befad5ce048391a7c07d40"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "0.10.11"

[[CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "ded953804d019afa9a3f98981d99b33e3db7b6da"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.0"

[[ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "dc7dedc2c2aa9faf59a55c622760a25cbefbe941"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.31.0"

[[CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[DataAPI]]
git-tree-sha1 = "ee400abb2298bd13bfc3df1c412ed228061a2385"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.7.0"

[[DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "4437b64df1e0adccc3e5d1adbc3ac741095e4677"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.9"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[DiffResults]]
deps = ["StaticArrays"]
git-tree-sha1 = "c18e98cba888c6c25d1c3b048e4b3380ca956805"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.0.3"

[[DiffRules]]
deps = ["NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "214c3fcac57755cfda163d91c58893a8723f93e9"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.0.2"

[[Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "a32185f5428d3986f47c2ab78b1f216d5e6cc96f"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.5"

[[Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[ExprTools]]
git-tree-sha1 = "b7e3d17636b348f005f11040025ae8c6f645fe92"
uuid = "e2ba6199-217a-4e67-a87a-7c52f15ade04"
version = "0.1.6"

[[FillArrays]]
deps = ["LinearAlgebra", "Random", "SparseArrays"]
git-tree-sha1 = "693210145367e7685d8604aee33d9bfb85db8b31"
uuid = "1a297f60-69ca-5386-bcde-b61e274b549b"
version = "0.11.9"

[[FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[Flux]]
deps = ["AbstractTrees", "Adapt", "CUDA", "CodecZlib", "Colors", "DelimitedFiles", "Functors", "Juno", "LinearAlgebra", "MacroTools", "NNlib", "NNlibCUDA", "Pkg", "Printf", "Random", "Reexport", "SHA", "Statistics", "StatsBase", "Test", "ZipFile", "Zygote"]
git-tree-sha1 = "0b3c6d0ce57d3b793eabd346ccc8f605035ef079"
uuid = "587475ba-b771-5e3f-ad9e-33799f191a9c"
version = "0.12.4"

[[ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "NaNMath", "Printf", "Random", "SpecialFunctions", "StaticArrays"]
git-tree-sha1 = "e2af66012e08966366a43251e1fd421522908be6"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.18"

[[Functors]]
deps = ["MacroTools"]
git-tree-sha1 = "a7bb2af991c43dcf5c3455d276dd83976799634f"
uuid = "d9f16b24-f501-4c13-a1f2-28368ffc5196"
version = "0.2.1"

[[GPUArrays]]
deps = ["AbstractFFTs", "Adapt", "LinearAlgebra", "Printf", "Random", "Serialization", "Statistics"]
git-tree-sha1 = "ececbf05f8904c92814bdbd0aafd5540b0bf2e9a"
uuid = "0c68f7d7-f131-5f86-a1c3-88cf8149b2d7"
version = "7.0.1"

[[GPUCompiler]]
deps = ["DataStructures", "ExprTools", "InteractiveUtils", "LLVM", "Libdl", "Logging", "TimerOutputs", "UUIDs"]
git-tree-sha1 = "03c3fb77362c08c3722bcef8dec488b708a95d52"
uuid = "61eb1bfa-7361-4325-ad38-22787b887f55"
version = "0.12.4"

[[IRTools]]
deps = ["InteractiveUtils", "MacroTools", "Test"]
git-tree-sha1 = "95215cd0076a150ef46ff7928892bc341864c73c"
uuid = "7869d1d1-7146-5819-86e3-90919afe41df"
version = "0.4.3"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "642a199af8b68253517b80bd3bfd17eb4e84df6e"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.3.0"

[[Juno]]
deps = ["Base64", "Logging", "Media", "Profile"]
git-tree-sha1 = "07cb43290a840908a771552911a6274bc6c072c7"
uuid = "e5e0dc1b-0480-54bc-9374-aad01c23163d"
version = "0.8.4"

[[LLVM]]
deps = ["CEnum", "Libdl", "Printf", "Unicode"]
git-tree-sha1 = "f57ac3fd2045b50d3db081663837ac5b4096947e"
uuid = "929cbde3-209d-540e-8aea-75f648917ca0"
version = "3.9.0"

[[LazyArtifacts]]
deps = ["Artifacts", "Pkg"]
uuid = "4af54fe1-eca0-43a8-85a7-787d91b784e3"

[[LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[LogExpFunctions]]
deps = ["DocStringExtensions", "LinearAlgebra"]
git-tree-sha1 = "7bd5f6565d80b6bf753738d2bc40a5dfea072070"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.2.5"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "6a8a2a625ab0dea913aba95c11370589e0239ff0"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.6"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[Media]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "75a54abd10709c01f1b86b84ec225d26e840ed58"
uuid = "e89f7d12-3494-54d1-8411-f7d8b9ae1f27"
version = "0.5.0"

[[Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "4ea90bd5d3985ae1f9a908bd4500ae88921c5ce7"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.0"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[NNlib]]
deps = ["Adapt", "ChainRulesCore", "Compat", "LinearAlgebra", "Pkg", "Requires", "Statistics"]
git-tree-sha1 = "7e6f31cfa39b1ff1c541cc8580b14b0ff4ba22d0"
uuid = "872c559c-99b0-510c-b3b7-b6c96a88d5cd"
version = "0.7.23"

[[NNlibCUDA]]
deps = ["CUDA", "LinearAlgebra", "NNlib", "Random", "Statistics"]
git-tree-sha1 = "5c970b9fd6098e5f04349b4d1a43fdd3e69c9e97"
uuid = "a00861dc-f156-4864-bf3c-e6376f28a68d"
version = "0.1.6"

[[NaNMath]]
git-tree-sha1 = "bfe47e760d60b82b66b61d2d44128b62e3a369fb"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "0.3.5"

[[NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[Preferences]]
deps = ["TOML"]
git-tree-sha1 = "00cfd92944ca9c760982747e9a1d0d5d86ab1e5a"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Random123]]
deps = ["Libdl", "Random", "RandomNumbers"]
git-tree-sha1 = "0e8b146557ad1c6deb1367655e052276690e71a3"
uuid = "74087812-796a-5b5d-8853-05524746bad3"
version = "1.4.2"

[[RandomNumbers]]
deps = ["Random", "Requires"]
git-tree-sha1 = "441e6fc35597524ada7f85e13df1f4e10137d16f"
uuid = "e6cf234a-135c-5ec9-84dd-332b85af5143"
version = "1.4.0"

[[Reexport]]
git-tree-sha1 = "5f6c21241f0f655da3952fd60aa18477cf96c220"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.1.0"

[[Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "4036a3bd08ac7e968e27c203d45f5fff15020621"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.1.3"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[SpecialFunctions]]
deps = ["ChainRulesCore", "LogExpFunctions", "OpenSpecFun_jll"]
git-tree-sha1 = "a50550fa3164a8c46747e62063b4d774ac1bcf49"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "1.5.1"

[[StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "a43a7b58a6e7dc933b2fa2e0ca653ccf8bb8fd0e"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.6"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[StatsAPI]]
git-tree-sha1 = "1958272568dc176a1d881acb797beb909c785510"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.0.0"

[[StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "2f6792d523d7448bbe2fec99eca9218f06cc746d"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.8"

[[TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[TimerOutputs]]
deps = ["ExprTools", "Printf"]
git-tree-sha1 = "209a8326c4f955e2442c07b56029e88bb48299c7"
uuid = "a759f4b9-e2f1-59dc-863e-4aeb61b1ea8f"
version = "0.5.12"

[[TranscodingStreams]]
deps = ["Random", "Test"]
git-tree-sha1 = "7c53c35547de1c5b9d46a4797cf6d8253807108c"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.9.5"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[ZipFile]]
deps = ["Libdl", "Printf", "Zlib_jll"]
git-tree-sha1 = "c3a5637e27e914a7a445b8d0ad063d701931e9f7"
uuid = "a5390f91-8eb1-5f08-bee0-b1d1ffed6cea"
version = "0.9.3"

[[Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[Zygote]]
deps = ["AbstractFFTs", "ChainRules", "ChainRulesCore", "DiffRules", "Distributed", "FillArrays", "ForwardDiff", "IRTools", "InteractiveUtils", "LinearAlgebra", "MacroTools", "NaNMath", "Random", "Requires", "SpecialFunctions", "Statistics", "ZygoteRules"]
git-tree-sha1 = "531474afbc343c3c7cb9b71c2771813c6defd550"
uuid = "e88e6eb3-aa80-5325-afca-941959d7151f"
version = "0.6.14"

[[ZygoteRules]]
deps = ["MacroTools"]
git-tree-sha1 = "9e7a1e8ca60b742e508a315c17eef5211e7fbfd7"
uuid = "700de1a5-db45-46bc-99cf-38207098b444"
version = "0.2.1"

[[nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═c5d23e34-e07c-11eb-326e-2b686ca192ea
# ╟─1f8df7e1-68a8-4e75-9a83-c0930e7fbe8a
# ╠═e9bf3ef8-a158-45eb-8190-c995a4bfad09
# ╠═57dfea0d-7e20-4dde-a900-61f48193ba3d
# ╠═6e788316-45d8-4ad4-ada3-7937acb47e39
# ╠═c820c798-1c45-436a-8ded-436524564165
# ╠═69fdab7d-1cc0-47d5-a822-f621ba2951e7
# ╟─124e9daa-1bfb-465e-a424-1f63cac4ba2b
# ╠═5af9940c-7d12-4f5f-bc9f-35fd214895cd
# ╠═8b73c8fb-da45-48d6-a13a-8d01618cfff0
# ╟─8172cd55-0a6c-4e52-923f-451f041f51c0
# ╠═9ffde980-cf5c-4eea-a4d8-902015ec9870
# ╠═9b54c388-3813-40f7-b4de-5bb001f5ebf9
# ╠═5f037ad7-e800-4ea8-842a-dc920c432d39
# ╠═bcb90d1f-c3a2-4e6d-9c1f-d297232664fc
# ╠═f200dafb-6e9c-4c7d-93a1-f4ecf08f7222
# ╟─cdce3049-7c3c-4c5b-931c-077579073a67
# ╟─dae9b292-e301-4472-b8be-75add4b162b3
# ╠═67862dba-616b-4d48-936d-f7bbcab8174e
# ╠═004b49f3-918d-4eeb-9ec5-2e19df59458a
# ╠═b53fae79-8b2f-432b-a735-784d64b74a2e
# ╠═587c6dc8-16d5-412d-bd91-4bff307ad3df
# ╠═e3909b31-6750-44d5-9d5c-66aa90024504
# ╠═2896c3d1-a6f4-45f0-b8f2-9f13b999a0a4
# ╟─c89dc98e-3b70-4ecc-ba57-7c763c8fe44d
# ╠═12e24b6b-9e73-4d23-85ff-82a416eea4ad
# ╟─a4a56238-2fe8-4e15-a9b5-886860eb167f
# ╠═90c4c99b-9b14-4de3-a6b6-0b7f9fd64b26
# ╠═c70a3712-2064-4d9c-b617-a11913cca2c1
# ╠═b0b3d82c-5e13-46de-b5ae-97914d14c6ff
# ╟─d5fc3d65-b8a7-490c-b264-f9c437efb029
# ╠═f3a27060-19cd-4001-ab32-883cdbb6d922
# ╠═d07adeeb-4808-4bd7-9bc0-c30a9fdc4b32
# ╠═15ef958d-38f7-4222-816c-6ab6c885c301
# ╠═72afaa8b-ca91-4196-af7c-be0b9be64267
# ╠═e192df9d-96f2-404e-9ad6-f7739400d336
# ╠═a56b9c3a-7359-4574-a098-c77c374b58a3
# ╠═4c11e7de-1a77-4783-8e95-03160caa0d6f
# ╟─41637e1c-a9aa-480f-bdca-b98d76935965
# ╠═321811df-c112-496e-a012-70b0e064c2f0
# ╠═35e0da79-c6d3-4bb0-8687-ed33e00dcbcb
# ╠═0e2dbdfb-98c9-4ca3-adc3-39fb03e12663
# ╠═bb50869e-3a13-484b-ace0-cddaa1bafa77
# ╠═ba7a424b-cfd5-4d4d-8832-92d0bb96215c
# ╟─a0d13bf2-2eea-438c-ba0e-c276116ec3b5
# ╠═bb60013d-d2be-4cfc-a0da-3675460aec11
# ╠═1dbb0bc5-ecd1-4d75-a31d-f842e3a197dc
# ╠═0bce1169-3bae-4381-bccc-2d50668c3d69
# ╟─2899b823-2d57-481d-99b8-0bdb6e57e763
# ╟─a6671a2b-67ae-4945-96c4-e0b97e9358ed
# ╠═a58960d8-11f3-43e6-937a-b55acdb1fa0a
# ╠═970dbcce-aec1-4a2b-9ea0-cdb2433326af
# ╟─dd767b4f-617a-42f4-b470-9b19214ab494
# ╠═81e609e5-2efd-40a2-ba34-7b56c6c1b9bc
# ╠═8091c7d0-36b6-4e35-a912-09a4543e896f
# ╠═0f29e2db-0d6e-4ad0-bfcd-025c049916a7
# ╟─97da2c64-3692-444d-8a48-7cad859db55b
# ╠═f418d0ae-0520-40e4-97f1-9acea12a2653
# ╠═9e01ef32-60fc-456f-8a0c-5927642d35b3
# ╟─364a8ecd-df4a-4fb3-b4f7-6f9d4cdf7ed6
# ╠═74407477-dde0-4145-b407-06e554eb8f56
# ╠═35254aa0-437c-4e7c-829b-66e22e29e1d1
# ╟─14ed535d-ccff-41c6-a711-df2b8f37037d
# ╠═54e261ee-1f25-4d38-be76-85042837ca75
# ╠═b79ff58c-ae91-4698-bbd5-4df306832c9e
# ╟─2b99e31a-f68d-49db-96a2-9b8201b79db2
# ╠═8f953e98-8d6c-4ee0-89a8-d4edda668bd5
# ╟─909acd1e-5890-4194-87ae-8d46882c2304
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
