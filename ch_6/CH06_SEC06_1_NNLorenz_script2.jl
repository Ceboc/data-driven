using Base: IteratorEltype
using Plots
using Parameters: @with_kw
using CUDA
if has_cuda()
	@info "CUDA is on"
	CUDA.allowscalar(false)
end
using Flux
using Flux: @epochs, mse, msle
using Flux.Data: DataLoader
using LinearAlgebra

using DiffEqFlux

using Distributed
addprocs()
@everywhere using Pkg
@everywhere Pkg.activate(".")
@everywhere using DifferentialEquations
@everywhere using ProgressMeter


# ODE definition
@everywhere function lorenz!(du,u,p,t)
	x,y,z = u
	σ,ρ,β = p
	du[1] = dx = σ*(y-x)
	du[2] = dy = x*(ρ-z)-y
	du[3] = dz = x*y - β*z
end

tspan = (0.0,8.0)			# time span
Δt = 0.01
t = tspan[1]:Δt:tspan[2]
p = [10.0,28.0,8/3]			# parameters of lorenz
u0 = 30 .*(rand(3,1).-0.5)	# initial point

# Declaration ODE problem
prob = ODEProblem(lorenz!,u0,tspan,p)

# Function to change the initial condition
@everywhere function prob_func(prob,i,repeat)
    remake(prob,u0=30 .*(rand(3,1).-0.5))
end

# Declare Ensemble Problem
ensemble_prob = EnsembleProblem(prob,prob_func=prob_func)

# Solve problem to 100 random initial points, each Δt = 0.01
sim = solve(ensemble_prob,Tsit5(),EnsembleDistributed(),trajectories=100,saveat=t)

sim_data = (p->p.u).(sim[:]) #Array(sim)

radbas(x) = inv(x^2)

# Neuronal ODE declaration 
dudt = Chain(Dense(3,10,logσ),Dense(10,10,radbas),Dense(10,10,tanh),Dense(10,3))	# Layers
n_ode = NeuralODE(dudt,tspan,Tsit5(),saveat=t)
ps = Flux.params(n_ode)

plot(sim,vars=(1,2,3),xlabel="x",ylabel="y",zlabel="z")

# Returns predicted values of neural ODE from initial points u0
function predict_n_ode()
	pred = map(n_ode,(p->p.prob.u0).(sim[:]))
	(p->p.u).(pred[:])
end

loss_n_ode() = sum(abs2,norm.(Flux.flatten(((p->p.u).(sim[:]) .- predict_n_ode()))))

data = Iterators.repeated((),100)

opt = ADAM(0.1)

cb = function ()
	display(loss_n_ode())
	#cur_pred = predict_n_ode()
	#pl = scatter(t,sim_data[1,:],label="data")
	#scatter!(pl,t,cur_pred[1,:],label="prediction")
	#display(plot(pl))
end

cb()

Flux.train!(loss_n_ode|>gpu,ps,data|>gpu,opt|>gpu,cb=cb)
