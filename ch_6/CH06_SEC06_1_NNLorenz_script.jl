using Base: Float32
using Plots: length
using Distributed
using Plots
using Parameters: @with_kw
using CUDA
if has_cuda()
	@info "CUDA is on"
	CUDA.allowscalar(false)
end

using Flux
using Flux: @epochs, mse, msle, crossentropy, mae, tversky_loss
using Flux.Data: DataLoader

addprocs()
@everywhere using Pkg
@everywhere Pkg.activate(".")
@everywhere using DifferentialEquations
@everywhere using ProgressMeter

# ODE definition
@everywhere function lorenz!(du,u,p,t)
	x,y,z = u
	σ,ρ,β = p
	du[1] = dx = σ*(y-x)
	du[2] = dy = x*(ρ-z)-y
	du[3] = dz = x*y - β*z
end

tspan = (0.0,8.0)
@everywhere tran = 0.0:0.01:8.0
l_tran = length(tran)
p = [10.0,28.0,8/3]
u0 = 30 .*(rand(3,1).-0.5)

prob = ODEProblem(lorenz!,u0,tspan,p)

# To change the initial condition
@everywhere function prob_func(prob,i,repeat)
    remake(prob,u0=30 .*(rand(3,1).-0.5))
end

# Declare Ensemble Problem
ensemble_prob = EnsembleProblem(prob,prob_func=prob_func)

# Solve problem to 100 random initial points
sim = solve(ensemble_prob,Tsit5(),EnsembleDistributed(),trajectories=1024,saveat=tran)

# Send solution to all workers
#@everywhere sim = $sim

# Plot solution
plot(sim,vars=(1,2,3),xlabel="x",ylabel="y",zlabel="z")

# Prepare evaluating points
#pts = ([i,j] for i in 1:l_tran-1, j in 1:100)

# Functions to evaluate x_k and x_(k+1)
#@everywhere sim_o(p) = sim[p[2]](tran[2:end][p[1]])
#@everywhere sim_i(p) = sim[p[2]](tran[1:end-1][p[1]])

# Evaluation to x_k (input) and k_(k+1) (output) as requiered to a NN
input = hcat((p->p.u).(sim[:]) ...)[1:end-1,:]#@showprogress pmap(sim_i,pts)
output = hcat((p->p.u).(sim[:]) ...)[2:end,:]#@showprogress pmap(sim_o,pts)

input = reshape(input,1,:)
output = reshape(output,1,:)

input = [getindex.(input,1);getindex.(input,2);getindex.(input,3)]
output = [getindex.(output,1);getindex.(output,2);getindex.(output,3)]

@with_kw mutable struct Args
	η::Float64 = 1.0e-1	# learning rate
	batchsize::Int= 24_000	# batch size
	epochs::Int = 10_000 	# number of epochs
	device::Function = gpu # set as gpu, if gpu available
end

train_data = DataLoader((input,output),batchsize=Args().batchsize)
train_data = Args().device.(train_data)

radbas(x) = inv(exp(x^2))
tre(x) = x^3

model = Chain(Dense(3,10),Dense(10,10,logσ),Dense(10,10,logσ),Dense(10,3))#Dense(10,10,radbas),Dense(10,3,logσ))
model = Args().device(model)
#model(sim[1].u[1])
#model(train_data[1][1][:,1])

loss(x,y) =  mse(model(x),y,agg=sum)

#loss(sim[1].u[1],sim[1].u[1])

function loss_all(dataloader,model)
	l = 0f0
	for (x,y) in dataloader
		l += loss(model(x),y)
	end
	1/length(dataloader)
end;

evalcb = function () 
	any(params(model)) do p
		any(isnan, p)
	end && Flux.stop()

	show(loss_all(train_data,model))
end

opt = ADAGrad(Args().η)

@epochs Args().epochs Flux.train!(loss,params(model),train_data,opt,cb=evalcb)

#model(sim[101].u[1]|>gpu)
m = model #|> cpu

u0_t = 30 .*(rand(3,1).-0.5)
prob_t = ODEProblem(lorenz!,u0_t,tspan,p)
sim_t = solve(prob_t,Tsit5(),saveat=tran)

pts = []
p0 = [sim_t.prob.u0]

for i in 1:8000-1
	pval = p0[i]
	@show ppar = m(pval|>gpu)
	push!(p0,ppar)
end

xs, ys, zs = getindex.(p0,1), getindex.(p0,2), getindex.(p0,3)

plot(sim_t,vars=(1,2,3),xlabel="x",ylabel="y",zlabel="z",label="Sim")
plot!(xs,ys,zs,label="Mod")
