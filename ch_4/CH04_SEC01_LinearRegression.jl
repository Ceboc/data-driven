### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 32087a54-ad34-11eb-0d10-61bca4f4a10d
using Plots, Optim, Polynomials, LaTeXStrings, PlutoUI

# ╔═╡ bf0650d1-8d97-4212-a942-a139dc5dfc32
@bind z Select(["With outlayers", "Whitout outlayers"])

# ╔═╡ bb1841e1-3bba-495e-9089-cec7a8394b71
begin
	x = 1.0:1.0:10.0
	if z == "With outlayers"
	y = [0.2, 0.5, 0.3, 3.5, 1.0, 1.5, 1.8, 2.0, 2.3, 2.2]
	elseif z == "Whitout outlayers"
	y = [0.2, 0.5, 0.3, 0.7, 1.0, 1.5, 1.8, 2.0, 2.3, 2.2]
	end
end

# ╔═╡ 3bdad7dd-2ab1-4c0e-91c7-cd7ab56c9da1
begin
	fit1(x0) = maximum(abs.(x0[2].*x.+x0[1].-y)) #Maximum error l∞
	fit2(x0) = sum(abs.(x0[2].*x.+x0[1].-y))	#Sum of absolute error l1
	fit3(x0) = sum(abs2.(x0[2].*x.+x0[1].-y))	#Least-squares error l2
end

# ╔═╡ 90f514d6-6879-4c1f-abc6-66118624a0ae
begin
	res1 = optimize(fit1,[0.2,0.2])
	res2 = optimize(fit2,[0.2,0.2])
	res3 = optimize(fit3,[0.2,0.2])
end

# ╔═╡ 49f939b7-6939-4e4b-aed7-bf63c0b042e3
begin
	v1 = Optim.minimizer(res1)
	v2 = Optim.minimizer(res2)
	v3 = Optim.minimizer(res3)
end

# ╔═╡ 296dc6ff-76bd-4be4-9dae-56f35b5360ec
begin
	pp1 = Polynomial(v1)
	pp2 = Polynomial(v2)
	pp3 = Polynomial(v3)

	xf = 0.0:0.1:11
	p1 = pp1.(xf)
	p2 = pp2.(xf)
	p3 = pp3.(xf)
end

# ╔═╡ c0efea8a-640b-4b84-9e2d-213a0e450757
begin
	plot(x,y,seriestype = :scatter,label="",title=z,ylim=[0,4])
	plot!(xf,p1,label=latexstring("E_\\infty"))
	plot!(xf,p2,label=latexstring("E_1"))
	plot!(xf,p3,label=latexstring("E_2"))
end

# ╔═╡ Cell order:
# ╠═32087a54-ad34-11eb-0d10-61bca4f4a10d
# ╠═bb1841e1-3bba-495e-9089-cec7a8394b71
# ╠═3bdad7dd-2ab1-4c0e-91c7-cd7ab56c9da1
# ╠═90f514d6-6879-4c1f-abc6-66118624a0ae
# ╠═49f939b7-6939-4e4b-aed7-bf63c0b042e3
# ╠═296dc6ff-76bd-4be4-9dae-56f35b5360ec
# ╠═c0efea8a-640b-4b84-9e2d-213a0e450757
# ╠═bf0650d1-8d97-4212-a942-a139dc5dfc32
