### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 9dfec030-ad50-11eb-2449-01bad77872c9
using Plots, Zygote, Optim, LinearAlgebra, PlutoUI, Roots

# ╔═╡ 45a8ebb4-6291-4fe1-87db-b49402b588a5
begin
	#Initial guess
	x = [3.0]
	y = [2.0]
	F(x,y) = (x.^2 + 3 * y.^2)[1]  #Function
	F(X) = F(X[1],X[2])
	f = [F(x,y)] # Initial function value
	for j in 1:10
		δ = (x[j]^2+9y[j]^2)/(2x[j]^2+54y[j]^2)
		push!(x,(1-2*δ)*x[j]) #update values
		push!(y,(1-6*δ)*y[j])
		push!(f,F(x[j+1],y[j+1]))
		
		if abs(f[j+1]-f[j])< 1e-6 #check convergence
			break
		end
	end
end

# ╔═╡ 8faded77-622d-4906-8570-2311c564b45b
begin
	xs = range(-3,3,length=100)
	ys = range(-3,3,length=100)
	xy = [[xs,ys] for xs in xs, ys in ys]
	
	fs = F.(xy)
end

# ╔═╡ 9b58ea33-576e-4b03-badf-db167b34cf75
begin
	surface(xs,ys,fs,alpha=0.7)
	plot3d!(x,y,f,xlabel="x",ylabel="y",line=:dot)
	scatter3d!(x,y,f)
end

# ╔═╡ e8bdea3f-a4cb-4d6a-8d12-f89bbf226b62
begin
	contour(xs,ys,fs)
	plot!(x,y,line=:dot)
	scatter!(x,y)
end

# ╔═╡ b4c6032f-d286-48df-9e39-7e4c9e20c484
begin
	f1(x,y) = @. 1.5 - 1.6*exp(-0.05*(3*(x+3)^2 + (y+3)^2))
	ff(x,y) = @. f1(x,y) + (0.5-exp(-0.1*3(3*(x-3)^2+(y-3)^2)))
	ff(X) = ff(X[1],X[2])
	
	∇ff(x,y) = collect(gradient(ff,x,y))
	∇ff(X) = ∇ff(X[1],X[2])
end

# ╔═╡ f389510a-80e1-44db-9b72-94c90adfdf62
@bind in_point Select(["[4.0,0.0]", "[0.0,-5.0]", "[-5.0,2.0]"])

# ╔═╡ 8ffb925f-d1ef-4101-9f3d-e6daf6645906
begin
	if in_point == "[4.0,0.0]"
		XX = [[4.0,0.0]] #Initial point
	elseif in_point == "[0.0,-5.0]"
		XX = [[0.0,-5.0]]#Initial point
	elseif in_point == "[-5.0,2.0]"
		XX = [[-5.0,2.0]] #Initial point
	end
	
	ffs =[ff(XX[1])] #Function value at initial point
	∇fs =[∇ff(XX[1])] #Gradient value at initial point

	for j in 1:10
		Xkp1(δ) = XX[j]-δ.*∇fs[j]  #function of next point
		FF(δ) = -(∇ff(Xkp1(δ))⋅∇fs[j]) #function that parametrizes δ
		#Seek interval of derivative roots
		resa = optimize(FF,[0.0],BFGS()) 
		resb = maximize(FF,[50.],BFGS())
		#root of derivative
		res11 = find_zero(FF,[resa.minimizer[1],resb.res.minimizer[1]]) 
		Xvp1 = Xkp1(res11) #next point
		push!(XX,Xvp1)
		push!(ffs,ff(Xvp1))
		push!(∇fs,∇ff(Xvp1))
		if abs(ffs[j+1]-ffs[j])<1e-6 #check convergence
			break
		end
	end
	
end

# ╔═╡ 1e63d1fa-ab15-4060-9c1f-e1619eeef236
begin
	xp = getindex.(XX,1)
	yp = getindex.(XX,2)
	
	xss = range(-6,6,length=100)
	yss = range(-6,6,length=100)
	xyss = ([xss,yss] for xss in xss, yss in yss)
	
	ffp = ff.(xyss)
end

# ╔═╡ 6d9ab3ea-405f-43b6-bcec-026723f6af23
begin
	scatter(xp,yp,ffs)
	plot3d!(xp,yp,ffs)
	surface!(xss,yss,ffp,alpha=0.5)
end

# ╔═╡ 12e13140-80bd-4f10-a61a-07096a9121d8
begin
	contour(xss,yss,ffp)
	plot!(xp,yp,line=:dot)
	scatter!(xp,yp)
end

# ╔═╡ 180e2e2a-ce8c-4bf6-ab52-7c86ca3723c0
begin 
	if in_point == "[4.0,0.0]"
		XX0 = [[4.0,0.0]] #Initial point
	elseif in_point == "[0.0,-5.0]"
		XX0 = [[0.0,-5.0]]#Initial point
	elseif in_point == "[-5.0,2.0]"
		XX0 = [[-5.0,2.0]] #Initial point
	end
	
	ffpp = [ff(XX0[1])]
	
	for i in 1:10
		mo = mod(i,2)
		if mo ==1
			yyp = getindex(XX0[i],2)
			fp(x) = ff(x,yyp)
			oy_r = optimize(x->fp(x[1]),[getindex(XX0[i],1)],BFGS())
			push!(XX0,[oy_r.minimizer[1],yyp])
			push!(ffpp,oy_r.minimum)
		elseif mo==0
			xxp = getindex(XX0[i],1)
			fp2(y) = ff(xxp,y)
			oy_r = optimize(y->fp2(y[1]),[getindex(XX0[i],2)],BFGS())
			push!(XX0,[xxp,oy_r.minimizer[1]])
			push!(ffpp,oy_r.minimum)
		end
			if abs(ff(XX0[i+1])-ff(XX0[i]))<1e-6
				break
			end
	end
end

# ╔═╡ 3fc389eb-91ea-4fbc-b444-c6da1b4cd90b
begin
	xx1 = getindex.(XX0,1)
	yy1 = getindex.(XX0,2)
end

# ╔═╡ 7cb1e162-d62b-4433-85d6-00e76f08beb0
begin
	scatter(xx1,yy1,ffpp)
	plot3d!(xx1,yy1,ffpp)
	surface!(xss,yss,ffp,alpha=0.5)
end

# ╔═╡ 9b550a2e-d150-4b50-9597-17c796ee43b5
begin
	contour(xss,yss,ffp)
	plot!(xx1,yy1,line=:dot)
	scatter!(xx1,yy1)
end

# ╔═╡ Cell order:
# ╠═9dfec030-ad50-11eb-2449-01bad77872c9
# ╠═45a8ebb4-6291-4fe1-87db-b49402b588a5
# ╠═8faded77-622d-4906-8570-2311c564b45b
# ╠═9b58ea33-576e-4b03-badf-db167b34cf75
# ╠═e8bdea3f-a4cb-4d6a-8d12-f89bbf226b62
# ╠═b4c6032f-d286-48df-9e39-7e4c9e20c484
# ╠═8ffb925f-d1ef-4101-9f3d-e6daf6645906
# ╠═1e63d1fa-ab15-4060-9c1f-e1619eeef236
# ╠═6d9ab3ea-405f-43b6-bcec-026723f6af23
# ╟─f389510a-80e1-44db-9b72-94c90adfdf62
# ╠═12e13140-80bd-4f10-a61a-07096a9121d8
# ╠═180e2e2a-ce8c-4bf6-ab52-7c86ca3723c0
# ╠═3fc389eb-91ea-4fbc-b444-c6da1b4cd90b
# ╠═7cb1e162-d62b-4433-85d6-00e76f08beb0
# ╠═9b550a2e-d150-4b50-9597-17c796ee43b5
