### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 8597f2bc-8ac0-11eb-2aaf-a385bd30e14e
using FFTW, Plots, LaTeXStrings, PlutoUI, OrdinaryDiffEq,ColorSchemes

# ╔═╡ a005e122-8ac0-11eb-2e2f-65929f3f340d
@bind a Slider(0.5:0.25:5,default=1,show_value=true) #Termal diffusivity constant

# ╔═╡ 8ca4303a-8ac1-11eb-3f27-6b19fea449c4
begin
	L = 100 #Length of domain
	N = 1000; #Number of discretization points
	dx = L/N;
	x = -L/2:dx:(L/2-dx)
end

# ╔═╡ a2ec013e-8ac1-11eb-0529-4d4a41a1a947
begin
	#Define discrete wavenumbers
	κ = 2*π/L .* (-N/2 : (N/2 -1))
	κ = fftshift(κ)
end

# ╔═╡ fc346f30-8ac1-11eb-0d63-dd3c8a83404b
begin
	#Initial condition
	u0 = zeros(length(x))
	u0[Int.(((L/2 -L/10)/dx) : ((L/2 + L/10)/dx))] .= 1.0
end

# ╔═╡ 4d461ba8-8ad6-11eb-1956-f1ce22f1c9c8
function dûdt(u,p,t)
	a,κ = p
	-a^2 .* κ.^2 .* u
end

# ╔═╡ 6823aab2-8ac2-11eb-1146-97e1431f2f29
begin
	#Simulate in Fourier frequency domain
	t = (0.0,10.0)
	û_prob = ODEProblem(dûdt,fft(u0),t,[a,κ])
	û = solve(û_prob,DP5())
end

# ╔═╡ a092786e-8adc-11eb-320d-9dc27caeeba9
begin
	su =length(û.t)
	u = zeros(size(û)).*im
	for k in 1:su
		u[:,k] .= ifft(û[:,k])
	end
end

# ╔═╡ bf49923e-8ae5-11eb-3040-217d98c97e60
lu = size(u[:,1:10:end])[2]

# ╔═╡ 9918c8cc-8ae6-11eb-3997-c3f596ea75ab
begin
	p = plot()
for i in 1:10:lu
 p = plot3d!(x,û.t[i].*ones(size(x)),real(u[:,i]),label="",linecolor=:temperaturemap,line_z=real(u[:,i]))
end
p
end

# ╔═╡ 7c2907ae-8b98-11eb-079e-3789fcd3d10e
begin
	xx = collect(x)
	uu = collect(û.t)
end

# ╔═╡ c6f19f12-8ae6-11eb-2e76-a9c0d16ed89d
heatmap(uu,xx,real(u),fillcolor=:temperaturemap,xlabel="t",ylabel="x")

# ╔═╡ Cell order:
# ╠═8597f2bc-8ac0-11eb-2aaf-a385bd30e14e
# ╠═a005e122-8ac0-11eb-2e2f-65929f3f340d
# ╠═8ca4303a-8ac1-11eb-3f27-6b19fea449c4
# ╠═a2ec013e-8ac1-11eb-0529-4d4a41a1a947
# ╠═fc346f30-8ac1-11eb-0d63-dd3c8a83404b
# ╠═4d461ba8-8ad6-11eb-1956-f1ce22f1c9c8
# ╠═6823aab2-8ac2-11eb-1146-97e1431f2f29
# ╠═a092786e-8adc-11eb-320d-9dc27caeeba9
# ╠═bf49923e-8ae5-11eb-3040-217d98c97e60
# ╠═9918c8cc-8ae6-11eb-3997-c3f596ea75ab
# ╠═7c2907ae-8b98-11eb-079e-3789fcd3d10e
# ╠═c6f19f12-8ae6-11eb-2e76-a9c0d16ed89d
