### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 766cb028-8f72-11eb-044f-b72de84d6848
using Wavelets, Images, FileIO, PyPlot, PlutoUI

# ╔═╡ 847cf7cc-8f72-11eb-2836-954809ef6e5d
A = load("dog.jpg")

# ╔═╡ 9b1d247a-8f72-11eb-02c8-4d456e1b8d80
B = Float64.(Gray.(A));

# ╔═╡ 9f24bf92-8f72-11eb-000a-a32df866f951
begin
	n =2
	C = dwt(B,wavelet(WT.db1),2)
end

# ╔═╡ a9d5e010-8f72-11eb-2a2d-e57e3104a6ba
begin
	imshow(C,cmap="gray",vmin=-0.25,vmax=0.75)
	gcf()
end

# ╔═╡ 45a8fbb8-8f77-11eb-070a-f7648cf1c428
@bind keep NumberField(0.002:0.002:0.1, default=0.1)

# ╔═╡ 1a74a316-8f77-11eb-07e6-19dfc99c7ba5
begin
	Csort = sort(abs.(C[:]))
	
	thresh = Csort[Int(floor((1-keep)*length(Csort)))]
	ind = abs.(C) .> thresh
	Cfilt = C.*ind
end

# ╔═╡ 9a6667d8-8f77-11eb-38bf-e36e2d4e1d7e
D = idwt(Cfilt,wavelet(WT.db1),2);

# ╔═╡ 89f68d14-8f77-11eb-3961-0f57c47c0c05
colorview(Gray,clamp01.(D))

# ╔═╡ Cell order:
# ╠═766cb028-8f72-11eb-044f-b72de84d6848
# ╠═847cf7cc-8f72-11eb-2836-954809ef6e5d
# ╠═9b1d247a-8f72-11eb-02c8-4d456e1b8d80
# ╠═9f24bf92-8f72-11eb-000a-a32df866f951
# ╠═a9d5e010-8f72-11eb-2a2d-e57e3104a6ba
# ╠═1a74a316-8f77-11eb-07e6-19dfc99c7ba5
# ╠═9a6667d8-8f77-11eb-38bf-e36e2d4e1d7e
# ╠═89f68d14-8f77-11eb-3961-0f57c47c0c05
# ╠═45a8fbb8-8f77-11eb-070a-f7648cf1c428
