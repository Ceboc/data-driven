a = 1;
L=100;
N = 1000;
dx = L/N;
x = -L/2:dx:L/2-dx;

kappa = (2*pi/L)*[-N/2:N/2-1];
kappa = fftshift(kappa);

u0 = 0*x;
u0((L/2-L/10)/dx:(L/2 + L/10)/dx) = 1;

t = 0:0.1:10;
[t,uhat] = ode45(@(t,uhat) rhsHeat(t,uhat,kappa,a),t,fft(u0));

for k = 1:length(t)
    u(k,:) = ifft(uhat(k,:));
end

figure, waterfall((u(1:10:end,:)));
figure, imagesc(flipud(u));
