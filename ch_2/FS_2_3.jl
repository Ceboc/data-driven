### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 0331ecf4-8aa9-11eb-1bc9-d9093edd5c7b
using Plots, FFTW, Distributions

# ╔═╡ 8b0400ae-8aa9-11eb-3d24-c9062aee39ac
begin
	dt = 0.001
	t = 0:dt:1
	fo = sin.(2*pi*50*t) .+ sin.(2*pi*120*t) #Sum of 2 frequencies
	f = fo .+ 2.5 .* rand(Uniform(-1,1),size(t)); # Add some noise
end

# ╔═╡ 9d9994ea-8aa9-11eb-1c22-ad10005940b8
# Compute the Fast Fourier Transform
begin
	n = length(t)
	fhat = fft(f) #Compute the Fast Fourier Transform
	PSD =fhat .* conj(fhat) / n #Power spectrum (power per freq)
	freq = 1 /(dt*n) .*(0:n) #Create x-axis of frequencies in Hz
	L =1:Int(floor(n/2)) # Only plot the first half of freqs
end

# ╔═╡ 3f01764a-8aac-11eb-2c0e-9d8a339889a2
#Use the PSD to filter aut noise
begin
	indices = abs.(PSD) .> 100 #Find all freqs with large power
	PSDclean = real(PSD) .* indices #Zero out all others
	fhat_filt = indices .* fhat #Zero out small Fourier coeffs. in y
	ffilt = ifft(fhat_filt) #Inverse FFT for filtered time signal
end

# ╔═╡ 8a1d4d3e-8aaf-11eb-10b5-99a182ed60cb
begin
	p1 = plot(t,fo,xlim=[0,0.25],label="Clean",xlabel="Time [s]",ylabel="f")
	plot!(t,f,label="Noisy")
	
	p2 = plot(t,fo,xlim=[0,0.25],label="Clean",xlabel="Time [s]",ylabel="f")
	plot!(t,real.(ffilt),label="Filtered")
	
	p3 = plot(freq[L],real(PSD[L]),label = "Noisy",xlabel="Frequency [Hz]",ylabel="PSD")
	plot!(freq[L],real(PSDclean[L]), label = "Clean")
	
	plot(p1,p2,p3,layout=(3,1))
end

# ╔═╡ Cell order:
# ╠═0331ecf4-8aa9-11eb-1bc9-d9093edd5c7b
# ╠═8b0400ae-8aa9-11eb-3d24-c9062aee39ac
# ╠═9d9994ea-8aa9-11eb-1c22-ad10005940b8
# ╠═3f01764a-8aac-11eb-2c0e-9d8a339889a2
# ╠═8a1d4d3e-8aaf-11eb-10b5-99a182ed60cb
