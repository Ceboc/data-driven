### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 9d2bdddc-8ea1-11eb-0af5-9193b2587451
using Plots

# ╔═╡ a193bc00-8ea1-11eb-3f85-0be611ea43b7
begin
	n = 256
	w = exp(-im*2π/n)
end

# ╔═╡ cb96b5fc-8ea1-11eb-2053-47e76d0e758b
DFT = zeros(n,n).*im

# ╔═╡ baa1ee06-8ea1-11eb-22c5-d37fb78c72ea
#slow
for i in 1:n
	for j in 1:n
		DFT[i,j] = w^((i-1)*(j-1))
	end
end

# ╔═╡ eeec2ab4-8ea1-11eb-1484-5b2e8e930089
#fast
DFT1 = (w^((i-1)*(j-1)) for i in 1:n, j in 1:n)

# ╔═╡ 0fcfb322-8ea2-11eb-277f-e78378dff3e0
heatmap(real.(DFT1))

# ╔═╡ Cell order:
# ╠═9d2bdddc-8ea1-11eb-0af5-9193b2587451
# ╠═a193bc00-8ea1-11eb-3f85-0be611ea43b7
# ╠═cb96b5fc-8ea1-11eb-2053-47e76d0e758b
# ╠═baa1ee06-8ea1-11eb-22c5-d37fb78c72ea
# ╠═eeec2ab4-8ea1-11eb-1484-5b2e8e930089
# ╠═0fcfb322-8ea2-11eb-277f-e78378dff3e0
