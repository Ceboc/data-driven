### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 99c337e6-8ab7-11eb-32bb-a3b1484c42d1
using FFTW, Plots, LaTeXStrings, PlutoUI

# ╔═╡ c93b4ba8-8abc-11eb-0d43-b54bccef02b9
begin
errDF = zeros(23)
errFFT = zeros(23)
end

# ╔═╡ 7b9b88ae-8abc-11eb-3275-35d6e5a2eb68
begin
	enes = range(10,Int(1e5),length=23)
end

# ╔═╡ b2c74f3c-8abc-11eb-07f7-6baf4abeb370
@bind nn Slider(enes,show_value=true)

# ╔═╡ b8753e6e-8ab7-11eb-10ec-5952d1cbf663
begin
	n=Int(nn)
	L = 30
	dx = L/(n)
	x = -L/2 : dx : L/2 -dx
	f = cos.(x) .* exp.(-x.^2/25)  #Function
	df = -(sin.(x) .* exp.(-x.^2/25)+(2/25)*x.*f)
end

# ╔═╡ 13e2fc82-8ab8-11eb-2ca4-4beed2def438
#Approximate derivative using finite Difference ...
begin
	dfFD = zeros(length(df)-1)
	for κ ∈ 1:length(df)-1
		dfFD[κ] = (f[κ+1]-f[κ])/dx
	end
	push!(dfFD,dfFD[end])
end

# ╔═╡ 87284a50-8ab9-11eb-2f05-d1d475389e9a
#Derivative using FFT (spectral derivative)
begin
	f̂=fft(f)
	κ = 2π/L .* (-n/2:n/2-1)
	κ = fftshift(κ) #Reorder fft frequencies
	df̂ = im * κ .* f̂
	dfFFT = real(ifft(df̂))
end

# ╔═╡ faa78740-8ab9-11eb-2943-9fc9bc524990
begin
	xl1 = latexstring("x")
	yl1 = latexstring("\frac{df}{dx}")
	plot(x,df,label="True derivative",xlabel=xl1,ylabel=yl1,color=:black,pallete=:tab10,lw=2)
	plot!(x,dfFD,label="Finite Difference",lw=2,pallete=:tab10,ls=:dash)
	plot!(x,dfFFT,label="FFT Derivative",lw=2,pallete=:tab10,ls=:dash)
end

# ╔═╡ 4f4ef962-8abb-11eb-16ef-17698246fc04
begin
	pos = findall(x->x==n,enes)[1]
	errDF[pos] = sqrt(sum((df.-dfFD).^2) * dx) / sqrt(sum((df).^2) * dx)
	errFFT[pos] = sqrt(sum((df.-dfFFT).^2) * dx) /  sqrt(sum((df).^2) * dx)
end

# ╔═╡ d7762bba-8abd-11eb-3473-0565ccef531e
begin
	plot(log10.(enes),log10.(errDF),shape=:circle)
	plot!(log10.(enes),log10.(errFFT),shape=:circle)
end

# ╔═╡ 7d5a03e4-8abe-11eb-080f-7f5a0ad5677d
errFFT

# ╔═╡ Cell order:
# ╠═99c337e6-8ab7-11eb-32bb-a3b1484c42d1
# ╠═b8753e6e-8ab7-11eb-10ec-5952d1cbf663
# ╠═13e2fc82-8ab8-11eb-2ca4-4beed2def438
# ╠═87284a50-8ab9-11eb-2f05-d1d475389e9a
# ╠═faa78740-8ab9-11eb-2943-9fc9bc524990
# ╠═c93b4ba8-8abc-11eb-0d43-b54bccef02b9
# ╠═4f4ef962-8abb-11eb-16ef-17698246fc04
# ╠═7b9b88ae-8abc-11eb-3275-35d6e5a2eb68
# ╠═b2c74f3c-8abc-11eb-07f7-6baf4abeb370
# ╠═d7762bba-8abd-11eb-3473-0565ccef531e
# ╠═7d5a03e4-8abe-11eb-080f-7f5a0ad5677d
