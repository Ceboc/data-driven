### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ e29dfeac-8ea3-11eb-2ed3-2f7c177881a1
using Plots, DSP

# ╔═╡ 7ed14510-8ea7-11eb-2fb9-3ba82aae0e84
begin
ω0 = 50
ω1 = 250
t1 = 2
end

# ╔═╡ 740f763a-8ea5-11eb-27b2-21f68ea0a5fa
ω(t) = ω0 + (ω1-ω0)t^2/(3t1^2)

# ╔═╡ 130089b0-8ea5-11eb-1b21-65758e22f743
f(t) = cos(2π*t*ω(t))

# ╔═╡ c8deaaf8-8ea7-11eb-38ae-6981d36d8e77
begin
st = 0.001
t = 0:st:2
end

# ╔═╡ d223a21c-8ea7-11eb-3e57-59107a922b76
x = f.(t);

# ╔═╡ 1e674232-8ea8-11eb-06b6-0f50aa46e03b
S=spectrogram(x,128,120,fs=1/st);

# ╔═╡ 3ee754b0-8ea9-11eb-0f51-853e1ece168a
heatmap(S.time,S.freq,log10.(S.power),ylim=[0,500],xlabel="Time [s]",ylabel="Frequency [Hz]")

# ╔═╡ b8ef0532-8ea9-11eb-0eaf-45c86915788f
S.time

# ╔═╡ Cell order:
# ╠═e29dfeac-8ea3-11eb-2ed3-2f7c177881a1
# ╠═7ed14510-8ea7-11eb-2fb9-3ba82aae0e84
# ╠═740f763a-8ea5-11eb-27b2-21f68ea0a5fa
# ╠═130089b0-8ea5-11eb-1b21-65758e22f743
# ╠═c8deaaf8-8ea7-11eb-38ae-6981d36d8e77
# ╠═d223a21c-8ea7-11eb-3e57-59107a922b76
# ╠═1e674232-8ea8-11eb-06b6-0f50aa46e03b
# ╠═3ee754b0-8ea9-11eb-0f51-853e1ece168a
# ╠═b8ef0532-8ea9-11eb-0eaf-45c86915788f
