### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 28f97616-8ead-11eb-04d7-bd6e3cf94ee5
using DSP, Plots, MAT

# ╔═╡ 783f718a-8ead-11eb-3237-097b74f240d6
begin
	music_file = matopen("beethoven_40sec.mat")
	music_vars = read(music_file)
	close(music_file)
end

# ╔═╡ e2e5d776-8eae-11eb-13e5-2ddc38cb056a
y,t,opts,nbits,fs = music_vars

# ╔═╡ 3285242c-8eae-11eb-1725-25ef1d68e52c
begin
	Y = y[2]
	T = t[2]
	NBITS = nbits[2]
	FS = fs[2]
end

# ╔═╡ 56569056-8eaf-11eb-259f-5b094b219f67
begin
	yy = Y[1:Int64(T*FS)] #First 40 seconds
	S = spectrogram(yy,5000,400,fs=FS)
end

# ╔═╡ 8db6c214-8eaf-11eb-19b6-afa19c3cc969
heatmap(S.time,S.freq,log10.(abs.(S.power)),fillcolor=:binary,ylim=[0,2000])

# ╔═╡ Cell order:
# ╠═28f97616-8ead-11eb-04d7-bd6e3cf94ee5
# ╠═783f718a-8ead-11eb-3237-097b74f240d6
# ╠═e2e5d776-8eae-11eb-13e5-2ddc38cb056a
# ╠═3285242c-8eae-11eb-1725-25ef1d68e52c
# ╠═56569056-8eaf-11eb-259f-5b094b219f67
# ╠═8db6c214-8eaf-11eb-19b6-afa19c3cc969
