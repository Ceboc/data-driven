### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 811fa66e-8a0f-11eb-1395-9fd7188b6f56
using Plots, PlutoUI, LaTeXStrings

# ╔═╡ a8e5b0e4-8a0f-11eb-168f-0d4d3b231c4f
# Define domain
begin
	dx = 0.001
	L = π
	x = ((-1+dx):dx:1).*L
	n = length(x)
	NN_max = 100
end

# ╔═╡ f59ca3fe-8a0f-11eb-1099-57fe8fe05d3d
# Define hat function
function f(x)
	if -π/2 ≤ x < 0
		1+2*x/π
	elseif 0 ≤ x < π/2
		1-2*x/π
	else
		0
	end
end

# ╔═╡ 71cfca0c-8a1b-11eb-287d-fd6c088a027e
begin
	fx = f.(x)
	A0 = sum(fx)*dx
end

# ╔═╡ bd083bbc-8aa5-11eb-1841-2f7a6aca896b
nf = sum(fx.^2)*dx

# ╔═╡ 827f0646-8a17-11eb-35ee-7f8d0b170879
# Compute fourier series
begin
	fFS = A0/2 * ones(n)
	fFS_a = zeros(n,NN_max)
	
	err = zeros(NN_max+1)
	err[1] = sum(abs.(fx-fFS).^2) *dx/nf
	
	A = zeros(NN_max)
	B = zeros(NN_max)
	
	for k ∈ 1:NN_max
		A[k] = sum(fx.*cos.(pi*k*x/L))*dx
		B[k] = sum(fx.*sin.(pi*k*x/L))*dx
		fFS .+= A[k] .* cos.(k.*pi.* x/L) + B[k] .* sin.(k .* pi .*x/L)
		
		fFS_a[:,k] = fFS
		err[k+1] = sum(abs.(fx-fFS).^2) *dx/nf
	end
end

# ╔═╡ dcde85a6-8aa2-11eb-195a-b1eab11355f6
begin
	Aₖ=zeros(NN_max+1)
	Aₖ[1] = A0
	Aₖ[2:end] .= A
end

# ╔═╡ 6dc41c16-8a1a-11eb-1d9d-733dc7189152
@bind N Slider(1:NN_max,show_value=true)

# ╔═╡ ac393b2a-8a10-11eb-0907-1ddaa66910e5
begin
	plot(x,f,label="Hat function",title="n= $N")
	plot!(x,fFS_a[:,N],label="Fourier approximation")
end

# ╔═╡ 8e887b66-8aa3-11eb-3469-e5debc9f21dc
begin
	ylab = latexstring("a_k")
	xlab = latexstring("k")
plot(0:NN_max,log10.(abs.(Aₖ)),xlabel=xlab,ylab=ylab)
scatter!([N],[log10(abs(Aₖ[N]))])
end

# ╔═╡ 0a3beb4e-8aa4-11eb-1343-75c7c93babb8
begin
	ylabe = latexstring("\\frac{|| f-\\hat{f}_k||}{||f||}") 
plot(0:NN_max,log10.(err),xlabel = xlab,ylabel=ylabe)
scatter!([N],[log10.(err[N])])
end

# ╔═╡ Cell order:
# ╠═811fa66e-8a0f-11eb-1395-9fd7188b6f56
# ╠═a8e5b0e4-8a0f-11eb-168f-0d4d3b231c4f
# ╠═f59ca3fe-8a0f-11eb-1099-57fe8fe05d3d
# ╠═71cfca0c-8a1b-11eb-287d-fd6c088a027e
# ╠═bd083bbc-8aa5-11eb-1841-2f7a6aca896b
# ╠═827f0646-8a17-11eb-35ee-7f8d0b170879
# ╠═ac393b2a-8a10-11eb-0907-1ddaa66910e5
# ╠═dcde85a6-8aa2-11eb-195a-b1eab11355f6
# ╠═6dc41c16-8a1a-11eb-1d9d-733dc7189152
# ╠═8e887b66-8aa3-11eb-3469-e5debc9f21dc
# ╠═0a3beb4e-8aa4-11eb-1343-75c7c93babb8
