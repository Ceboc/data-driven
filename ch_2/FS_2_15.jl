### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ ea63a6a2-8f5a-11eb-081e-f186a8e4da66
using FFTW, Images, FileIO, LinearAlgebra, PlutoUI

# ╔═╡ c7fa9e3c-8f6d-11eb-0968-97b54feaabc7
using Plots

# ╔═╡ 43668906-8f5b-11eb-1192-7b7f29637a8b
A = load("dog.jpg")

# ╔═╡ f16e5030-8f5c-11eb-2d2a-63e074c704b5
B = Float64.(Gray.(A));

# ╔═╡ 5762fcec-8f5d-11eb-10b0-c1b781b58e0b
size(B,2)

# ╔═╡ 6c8fd242-8f5e-11eb-3dde-d9ffe334eaab
function fft_row(B)
	Cshift = zeros(size(B)).*im
	C = zeros(size(B)).*im
	for i in 1:size(B,1)
		Cshift[i,:] .= (fftshift∘fft)(B[i,:])
		C[i,:] .= fft(B[i,:])
	end
	
	return Cshift, C
end

# ╔═╡ c0f093f8-8f5e-11eb-0460-590163372198
Cshift,C = fft_row(B);

# ╔═╡ 05279576-8f5f-11eb-3bc7-4f33cf0a0a2f
colorview(Gray,clamp01.(log10.(abs.(Cshift))./maximum(log10.(abs.(Cshift)))))

# ╔═╡ 426a6318-8f65-11eb-276e-699e9064c3bb
D = zeros(ComplexF64,size(C));

# ╔═╡ 625298dc-8f65-11eb-3af7-c966c9688667
for j in 1:size(C,2)
	D[:,j] = fft(C[:,j]) 
end

# ╔═╡ 81323558-8f65-11eb-0c11-ab5de5749fea
colorview(Gray,clamp01.(log10.(abs.(fftshift(D)))./maximum(log10.(abs.(fftshift(D))))))

# ╔═╡ 57a39e6a-8f66-11eb-21ad-3706084dbd13
colorview(Gray,clamp01.(log.(abs.(fftshift(fft(B))))./maximum(log.(abs.(fftshift(fft(B)))))))

# ╔═╡ 8ccac7b2-8f66-11eb-3c8a-61ed912ec94b
begin
	Bt = fft(B);
	#]Bt = Bt/maximum(abs.(Bt))
end

# ╔═╡ 4f693ae2-8f67-11eb-0f11-c1425952ce95
Bt_sort = sort(abs.(Bt[:]))

# ╔═╡ a4479692-8f66-11eb-32f2-6f197b14d02e
@bind keep NumberField(0.002:0.002:0.1, default=0.1)

# ╔═╡ e13970aa-8f66-11eb-3003-3729d3af6ba2
begin
	thresh = Bt_sort[Int(floor((1-keep)*length(Bt_sort)))]
	ind = abs.(Bt) .> thresh
	Atlow = Bt .* ind
	Alow = abs.(ifft(Atlow))
end

# ╔═╡ f6062b6c-8f67-11eb-3e5b-21a7a1670ac9
colorview(Gray,clamp01.(Alow))

# ╔═╡ d6df938c-8f69-11eb-1d88-c7af9f0f7f06
begin
	Bnoise = B .+  .2 .* (randn(size(B)).-1);
	
	Btt = fft(Bnoise)
	F = log.(abs.(Btt) .+ 1.0 )
end

# ╔═╡ 7031354c-8f6a-11eb-3b85-39d9bdccb458
colorview(Gray,clamp01.(Bnoise))

# ╔═╡ 0e448e96-8f6b-11eb-054a-656edc179101
colorview(Gray,clamp01.(fftshift(F) ./ maximum(F)))

# ╔═╡ ad566768-8f6e-11eb-0c03-ef8e70f89826
@bind RR NumberField(0:10:1000, default=150)

# ╔═╡ 442b710a-8f6b-11eb-352d-7341364ad954
begin
	nx,ny = size(B)
	ys = range(-ny/2+1,ny/2,length=ny)
	xs = range(-nx/2+1,nx/2,length=nx)
	R2 = (xs^2 + ys^2 for ys in ys, xs in xs)
	ind1 = (R2 .< RR^2)'
end

# ╔═╡ 70fdcc68-8f6c-11eb-04dd-739fbd1e1760
Bfilt_fft = fftshift(Btt).*ind1;

# ╔═╡ 92f0ebc8-8f6c-11eb-231b-e95a51d523ab
colorview(Gray,clamp01.(log.(abs.(Bfilt_fft))./maximum(log.(abs.(Bfilt_fft)))))

# ╔═╡ f61edb44-8f6c-11eb-1d04-05fdd56604c1
Bfilt=ifft(ifftshift(Bfilt_fft));

# ╔═╡ caedd186-8f6d-11eb-1a1f-b116b504a0df
colorview(Gray,clamp01.(real.(Bfilt)))

# ╔═╡ Cell order:
# ╠═ea63a6a2-8f5a-11eb-081e-f186a8e4da66
# ╠═43668906-8f5b-11eb-1192-7b7f29637a8b
# ╠═f16e5030-8f5c-11eb-2d2a-63e074c704b5
# ╠═5762fcec-8f5d-11eb-10b0-c1b781b58e0b
# ╠═6c8fd242-8f5e-11eb-3dde-d9ffe334eaab
# ╠═c0f093f8-8f5e-11eb-0460-590163372198
# ╠═05279576-8f5f-11eb-3bc7-4f33cf0a0a2f
# ╠═426a6318-8f65-11eb-276e-699e9064c3bb
# ╠═625298dc-8f65-11eb-3af7-c966c9688667
# ╠═81323558-8f65-11eb-0c11-ab5de5749fea
# ╠═57a39e6a-8f66-11eb-21ad-3706084dbd13
# ╠═8ccac7b2-8f66-11eb-3c8a-61ed912ec94b
# ╠═4f693ae2-8f67-11eb-0f11-c1425952ce95
# ╠═e13970aa-8f66-11eb-3003-3729d3af6ba2
# ╠═a4479692-8f66-11eb-32f2-6f197b14d02e
# ╠═f6062b6c-8f67-11eb-3e5b-21a7a1670ac9
# ╠═d6df938c-8f69-11eb-1d88-c7af9f0f7f06
# ╠═7031354c-8f6a-11eb-3b85-39d9bdccb458
# ╠═0e448e96-8f6b-11eb-054a-656edc179101
# ╠═442b710a-8f6b-11eb-352d-7341364ad954
# ╠═70fdcc68-8f6c-11eb-04dd-739fbd1e1760
# ╠═92f0ebc8-8f6c-11eb-231b-e95a51d523ab
# ╟─ad566768-8f6e-11eb-0c03-ef8e70f89826
# ╠═f61edb44-8f6c-11eb-1d04-05fdd56604c1
# ╠═c7fa9e3c-8f6d-11eb-0968-97b54feaabc7
# ╠═caedd186-8f6d-11eb-1a1f-b116b504a0df
