### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 29955102-8812-11eb-104e-5f044e2002a6
using LinearAlgebra, Images, FileIO, PlutoUI

# ╔═╡ 9445ce7a-8812-11eb-054f-d9f02b000545
function rsvd(X,r,q,p)
	#Step 1: Sample column space of X with P matrix
	ny = size(X,2)
	P = rand(ny,r+p)
	Z = X*P
	for k in 1:q
		Z = X*(X'*Z)
	end
	
	Q, R = qr(Z)
	
	#Step 2: Compute SVD on projected Y = Q'*X
	Y = Q' * X
	Uy, Σ, V = svd(Y)
	U = Q*Uy
	return U, Σ, V
end

# ╔═╡ 4d1dee82-8813-11eb-1eec-594ff6781faf
A = load("jupiter.jpg")

# ╔═╡ 2ef77c10-8814-11eb-182b-abe52630a252
X = Gray.(A)

# ╔═╡ 43d2ed0e-8814-11eb-298b-97875c9cb6a5
U,Σ,V = svd(X);

# ╔═╡ 530687ec-8814-11eb-1426-3300885e77dd
@bind r Slider(1:1000,show_value=true,default=400) #Target rank

# ╔═╡ a8a9d152-8814-11eb-1c3a-d54c6bace324
@bind q Slider(1:10,show_value=true,default=1) # Power iterations

# ╔═╡ c1e05c4a-8814-11eb-2b17-99c91b55dac3
@bind p Slider(1:10,show_value=true,default=5) # Oversampling parameter

# ╔═╡ ea2f2898-8814-11eb-1561-f5e0b43adcff
rU, rΣ, rV = rsvd(X,r,q,p);

# ╔═╡ 321d12dc-8815-11eb-0a49-01f8aef8d21e
begin
	XSVD = U[:,1:r]*Diagonal(Σ[1:r])*V[:,1:r]' #SVD approx
	errSVD = norm(X - XSVD,2)/norm(X,2)
	XrSVD = rU[:,1:r]*Diagonal(rΣ[1:r])*rV[:,1:r]'
	errrSVD = norm(X - XrSVD,2)/norm(X,2)
end

# ╔═╡ 0fd6018e-8815-11eb-0365-89252f83a743
mosaicview(X,Gray.(XSVD),Gray.(XrSVD),nrow=1)

# ╔═╡ Cell order:
# ╠═29955102-8812-11eb-104e-5f044e2002a6
# ╠═9445ce7a-8812-11eb-054f-d9f02b000545
# ╟─4d1dee82-8813-11eb-1eec-594ff6781faf
# ╠═2ef77c10-8814-11eb-182b-abe52630a252
# ╠═43d2ed0e-8814-11eb-298b-97875c9cb6a5
# ╠═ea2f2898-8814-11eb-1561-f5e0b43adcff
# ╠═321d12dc-8815-11eb-0a49-01f8aef8d21e
# ╠═530687ec-8814-11eb-1426-3300885e77dd
# ╠═a8a9d152-8814-11eb-1c3a-d54c6bace324
# ╠═c1e05c4a-8814-11eb-2b17-99c91b55dac3
# ╠═0fd6018e-8815-11eb-0365-89252f83a743
