### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ d4aaa98e-877e-11eb-37fd-13640bccaedb
using LinearAlgebra, Plots, PlutoUI, Images, CoordinateTransformations, Rotations, LaTeXStrings, CUDA

# ╔═╡ ec1c06e4-877e-11eb-0a07-010f3e671a7a
begin
	n = 1000
	Xm = zeros(n,n)
	Xm[Int(n/4):3*Int(n/4),Int(n/4):3Int(n/4)] .= 1
	
	X = Gray.(Xm)
end

# ╔═╡ 67ca960c-877f-11eb-09e3-79aed9031935
@bind rot Slider(0:0.1:π,show_value=true)

# ╔═╡ 7d1f2368-877f-11eb-1eb6-c93cb03ac00e
Y = imrotate(X,rot,axes(X))

# ╔═╡ c118c0be-8782-11eb-35d1-19b26b698658
Ux, Σx, Vx = svd(X);

# ╔═╡ 77a1bb82-8784-11eb-1f26-09b95c6e8f23
begin
	Ym = zeros(size(Y))
	Ym[findall(x->isnan(x),convert(Array{Float64},Y))] .= 0.0
	Ym[findall(x->(1.0==x),convert(Array{Float64},Y))] .= 1.0
end

# ╔═╡ ce11eaac-8782-11eb-2416-1fa1f3a7ccb4
Uy, Σy, Vy = svd(Ym);

# ╔═╡ e8daf060-8782-11eb-032d-07d63fb47712
begin
plot(log10.(Σx),label=latexstring("\\theta = 0"),markershapes=:circle,ylabel=latexstring("\\textrm{Singular Value}\\; \\sigma_r"),xlabel=latexstring("r"))
plot!(log10.(Σy),c=:red,label=latexstring("\\theta = $rot"),markershapes=:circle)
end

# ╔═╡ Cell order:
# ╠═d4aaa98e-877e-11eb-37fd-13640bccaedb
# ╠═ec1c06e4-877e-11eb-0a07-010f3e671a7a
# ╠═7d1f2368-877f-11eb-1eb6-c93cb03ac00e
# ╠═67ca960c-877f-11eb-09e3-79aed9031935
# ╠═c118c0be-8782-11eb-35d1-19b26b698658
# ╠═ce11eaac-8782-11eb-2416-1fa1f3a7ccb4
# ╠═e8daf060-8782-11eb-032d-07d63fb47712
# ╠═77a1bb82-8784-11eb-1f26-09b95c6e8f23
