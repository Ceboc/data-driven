### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 74975ae6-7c94-11eb-1711-85f39d73fa8c
using LinearAlgebra, Statistics, Plots

# ╔═╡ b9fed58a-7c94-11eb-31ac-f9a96e0735ec
begin
	xC = [2; 1] 		#Center of data
	sig = [2; 0.5]		#Principal axes
	
	θ = π/3 			#Rotate cloud π/3
	R = [cos(θ) -sin(θ);#Rotation matrix
		 sin(θ) cos(θ)]	
	
	nPoints = 10000
	X = R*Diagonal(sig)*randn(2,nPoints) + Diagonal(xC)*ones(2,nPoints)
end

# ╔═╡ 17a241c8-7c96-11eb-08d3-3362facced5f
begin
	Xavg = mean(X,dims=2)			#Compute mean
	B = X - Xavg*ones(1,nPoints)	#Mean substracted data
	U, Σ, V = svd(B/sqrt(nPoints))
end

# ╔═╡ 2148929e-7c97-11eb-0029-217c9d3f95b7
θs = collect(0:0.01:1) .* 2.0 .* π;

# ╔═╡ a0c15e0a-7c97-11eb-25a5-8fd1a09b7fdc
Xstd = U*Diagonal(Σ)*hcat(cos.(θs),sin.(θs))'; # 1-std conf. interval

# ╔═╡ bc92e524-7c95-11eb-2032-5b6daf6a13ae
begin
	scatter(X[1,:],X[2,:],label="",xlabel = "x", ylabel = "y")
	plot!(Xavg[1].+Xstd[1,:],Xavg[2].+Xstd[2,:],color=:red,label="",linewidth=2)
	plot!(Xavg[1].+2Xstd[1,:],Xavg[2].+2Xstd[2,:],color=:red,label="",linewidth=2)
	plot!(Xavg[1].+3Xstd[1,:],Xavg[2].+3Xstd[2,:],color=:red,label="",linewidth=2)
end

# ╔═╡ Cell order:
# ╠═74975ae6-7c94-11eb-1711-85f39d73fa8c
# ╠═b9fed58a-7c94-11eb-31ac-f9a96e0735ec
# ╠═bc92e524-7c95-11eb-2032-5b6daf6a13ae
# ╠═17a241c8-7c96-11eb-08d3-3362facced5f
# ╠═2148929e-7c97-11eb-0029-217c9d3f95b7
# ╠═a0c15e0a-7c97-11eb-25a5-8fd1a09b7fdc
