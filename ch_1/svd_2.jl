### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 54ff6c86-7af1-11eb-135a-ff7551d33aae
using LinearAlgebra, WGLMakie, AbstractPlotting

# ╔═╡ 1cc7e80e-7af1-11eb-22fd-b1470ec469dd
md"""
# Singular Value Decomposition
## Geometric interpretation
"""

# ╔═╡ 373cf8e2-7af1-11eb-0f2e-4d7670be0d3e
begin
θ = [π/15; -π/9; -π/20]
Σ = diagm([3.0; 1.0; 0.5])  #scale x, y, and z
end

# ╔═╡ 7476750a-7af1-11eb-38f3-57cb6895b8f2
begin
Rx = [1.0 0.0 0.0;
	  0.0 cos(θ[1]) -sin(θ[1]);
	  0.0 sin(θ[1]) cos(θ[1])] 	#rotate about x-axis
Ry = [cos(θ[2]) 0.0 sin(θ[2]);
	  0.0 1.0 0.0;
	  -sin(θ[2]) 0.0 cos(θ[2])] #rotate about y-axis
Rz = [cos(θ[3]) -sin(θ[3]) 0.0;
	  sin(θ[3]) cos(θ[3]) 0.0;
	  0.0 0.0 1.0]				#rotate about z-axis
X = Rz*Ry*Rx*Σ
end

# ╔═╡ aab87a18-7af2-11eb-3fd0-8d8dcee9a59c
begin
	θs = range(0.0,2π,length=100)
	ϕs = range(0.0,π,length=20)
	r=25
	x = [r*cos(φ)*sin(ϑ) for ϑ in θs, φ in ϕs]
	y = [r*sin(φ)*sin(ϑ) for ϑ in θs, φ in ϕs]
	z = [r*cos(ϑ) for ϑ in θs, φ in ϕs]
end

# ╔═╡ efab0a3a-7af4-11eb-2026-d96e224cf801
surface(x,y,z,colormap = :jet1)

# ╔═╡ 0aea8a82-7af8-11eb-17f6-a1bc020a656e
begin
	xR = 0 * x;
	yR = 0 * y;
	zR = 0 * z;
	
	ly,lx = size(x)
	for i in 1:ly
		for j in 1:lx
			vecR = X*[x[i,j];y[i,j];z[i,j]]
			xR[i,j] = vecR[1]
			yR[i,j] = vecR[2]
			zR[i,j] = vecR[3]
		end
	end
end

# ╔═╡ a92d4196-7af8-11eb-382f-276212a3c087
surface(xR,yR,z,colormap=:jet1)

# ╔═╡ Cell order:
# ╠═54ff6c86-7af1-11eb-135a-ff7551d33aae
# ╟─1cc7e80e-7af1-11eb-22fd-b1470ec469dd
# ╠═373cf8e2-7af1-11eb-0f2e-4d7670be0d3e
# ╠═7476750a-7af1-11eb-38f3-57cb6895b8f2
# ╠═aab87a18-7af2-11eb-3fd0-8d8dcee9a59c
# ╠═efab0a3a-7af4-11eb-2026-d96e224cf801
# ╠═0aea8a82-7af8-11eb-17f6-a1bc020a656e
# ╠═a92d4196-7af8-11eb-382f-276212a3c087
