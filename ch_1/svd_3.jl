### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 3238cece-7bbf-11eb-0151-69dc376297a6
using LinearAlgebra, Plots, CSV, DataFrames

# ╔═╡ 44f37dc2-7bbf-11eb-0b4f-d9361fa93c8f
begin
	x = 3;
	a = (-2.0:0.25:2.0)
	a = ones(1,length(a))' .* a
	b = a * x .+ 1*randn(size(a))
end

# ╔═╡ 9df1afc4-7bc0-11eb-19cc-c3d557af4805
U, Σ, V = svd(a);

# ╔═╡ efda8c06-7bc1-11eb-0471-ebb4fd528804
x̃ = V*Diagonal(Σ)^-1*U'*b;

# ╔═╡ 9f781f14-7bbf-11eb-2439-f54fc3bab034
begin
plot(a,x*a,xlabel="a",ylabel="b",color=:black,label="True line")
plot!(a,b,seriestype=:scatter,color="red",shape=:x,label = "Noisy data")
plot!(a,x̃.*a,style=:dash,label="Regression line")	
end

# ╔═╡ Cell order:
# ╠═3238cece-7bbf-11eb-0151-69dc376297a6
# ╠═44f37dc2-7bbf-11eb-0b4f-d9361fa93c8f
# ╠═9f781f14-7bbf-11eb-2439-f54fc3bab034
# ╠═9df1afc4-7bc0-11eb-19cc-c3d557af4805
# ╠═efda8c06-7bc1-11eb-0471-ebb4fd528804
