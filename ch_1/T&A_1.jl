### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ af363932-8776-11eb-120b-a126765ed07a
using LinearAlgebra, Plots, PlutoUI

# ╔═╡ 01fa13d2-8777-11eb-0836-6d5279c6843c
t = (-3:0.01:3)

# ╔═╡ 25e37950-8777-11eb-2bf5-b7e6e81ca38d
#Compute the underlyig low-rank signal
begin 
	Utrue = [cos.(17.0*t).*exp.(-t.^2) sin.(11.0*t)]
	Strue = [2.0 0.0; 0.0 .5]
	Vtrue = [sin.(5.0*t).*exp.(-t.^2) cos.(13.0*t)]
	
	X = Utrue * Strue * Vtrue'
end

# ╔═╡ 8f1f774a-8777-11eb-3110-d9b1f0b8478a
heatmap(X)

# ╔═╡ 9a590af4-8777-11eb-13ae-8db344a0c280
#Contaminate the signal with noise
begin
	sigma = 1
	Xnoisy = X .+ sigma*rand(Float64,size(X))
end

# ╔═╡ 0cbdf526-8778-11eb-364e-8b206d8c6e00
heatmap(Xnoisy)

# ╔═╡ 2696888c-8778-11eb-08fd-83bc9fce224a
#Truncate using optimal hard threshold
begin
	U, Σ, V = svd(Xnoisy)
	N = size(Xnoisy,1)
	cutoff = (4/sqrt(3)) * sqrt(N) * sigma; #Hard threshold
	r = maximum(findall(x -> x>cutoff,Σ)) #Keep modes w/ sig > cutoff
	
	Xclean = U[:,1:r]*Diagonal(Σ)[1:r,1:r]*V[:,1:r]'
end

# ╔═╡ 9d76201a-8779-11eb-2a9d-db16f86170c9
heatmap(Xclean)

# ╔═╡ b8cd92b6-877a-11eb-2dba-1fa2a3607a9b
@bind cutoff_var Slider(0:0.01:1,show_value=true)

# ╔═╡ 6685d1e0-8779-11eb-1b1e-e178c0cca1e0
#truncate using the 90% energy criterion
begin
	cdS = cumsum(Σ) ./ sum(Σ)
	rv = minimum(findall(x -> x>cutoff_var,cdS))
	
	Xv = U[:,1:rv]*Diagonal(Σ)[1:rv,1:rv]*V[:,1:rv]'
end

# ╔═╡ 950def52-8779-11eb-3e67-a94018a6462a
heatmap(Xv)

# ╔═╡ d49054e6-877c-11eb-09f9-cbb67d549984
begin
plot(log10.(Σ))
plot!(log10.(Σ[1:r]))
end

# ╔═╡ Cell order:
# ╠═af363932-8776-11eb-120b-a126765ed07a
# ╠═01fa13d2-8777-11eb-0836-6d5279c6843c
# ╠═25e37950-8777-11eb-2bf5-b7e6e81ca38d
# ╠═8f1f774a-8777-11eb-3110-d9b1f0b8478a
# ╠═9a590af4-8777-11eb-13ae-8db344a0c280
# ╠═0cbdf526-8778-11eb-364e-8b206d8c6e00
# ╠═2696888c-8778-11eb-08fd-83bc9fce224a
# ╠═9d76201a-8779-11eb-2a9d-db16f86170c9
# ╠═6685d1e0-8779-11eb-1b1e-e178c0cca1e0
# ╠═b8cd92b6-877a-11eb-2dba-1fa2a3607a9b
# ╠═950def52-8779-11eb-3e67-a94018a6462a
# ╠═d49054e6-877c-11eb-09f9-cbb67d549984
