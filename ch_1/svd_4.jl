### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ abd1ccdc-7bca-11eb-11b5-0fce9989efd4
using LinearAlgebra, Plots, CSV, DataFrames

# ╔═╡ 4806c9f2-7bcd-11eb-33ec-95bfba8ead8e
begin
	a_cement = convert(Matrix,CSV.read("hald_ingredients.csv",DataFrame,header=false))
	b_cement = convert(Matrix,CSV.read("hald_heat.csv",DataFrame,header=false))
end

# ╔═╡ 4d3d373c-7bcd-11eb-2a61-f3997e850477
Uc, Sc, Vc = svd(a_cement);

# ╔═╡ 57563e6a-7bcd-11eb-0590-f7fa0a19839f
xc = Vc*inv(Diagonal(Sc))*Uc'*b_cement; #Remember that Julia only returns a list of singular values not a matrix

# ╔═╡ 7891318e-7bcd-11eb-3f9d-eb0c0bf9e448
begin
	plot(b_cement,xlabel="Mixture",ylabel="Heat [cal/gram]",label="Heat data")
	plot!(a_cement*xc,label="Regression")
end

# ╔═╡ Cell order:
# ╠═abd1ccdc-7bca-11eb-11b5-0fce9989efd4
# ╠═4806c9f2-7bcd-11eb-33ec-95bfba8ead8e
# ╠═4d3d373c-7bcd-11eb-2a61-f3997e850477
# ╠═57563e6a-7bcd-11eb-0590-f7fa0a19839f
# ╠═7891318e-7bcd-11eb-3f9d-eb0c0bf9e448
