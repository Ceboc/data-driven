### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 89e99f76-8851-11eb-3419-1774ef406388
using LinearAlgebra, Plots, TensorDecompositions

# ╔═╡ a58e9ef2-8851-11eb-3315-83f0bcc1c372
f(X) = exp(-X[1]^2-0.5*X[2]^2)cos(2*X[3])+sech(X[1])*tanh(X[1])exp(-0.2X[2]^2)sin(X[3]) 

# ╔═╡ fc8d889e-8851-11eb-1194-9b8316835847
begin
	x = -5:0.1:5
	y = -6:0.1:6
	t = 0:0.1:10*pi
	
	pos = ([x,y,t] for x in x, y in y, t in t)
end

# ╔═╡ 76cc387e-8852-11eb-0885-efdc2e6fd03c
A = f.(pos);

# ╔═╡ 22e654be-8852-11eb-1519-d9b38f8eda4c
candecomp(A, 2)

# ╔═╡ Cell order:
# ╠═89e99f76-8851-11eb-3419-1774ef406388
# ╠═a58e9ef2-8851-11eb-3315-83f0bcc1c372
# ╠═fc8d889e-8851-11eb-1194-9b8316835847
# ╠═76cc387e-8852-11eb-0885-efdc2e6fd03c
# ╠═22e654be-8852-11eb-1519-d9b38f8eda4c
