### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 5a97aed2-9ff5-11eb-3a71-9fda2f44e8a6
using Convex, SCS, Plots, LinearAlgebra, Distributions

# ╔═╡ 2560003b-00d8-48c5-851d-a0bc18a2a003
begin
	x = sort(rand(Uniform(-2.0,2.0),25)) #Random data from [-2,2]
	b = 0.9*x + 0.1 * randn(size(x)) #Line y =.9x with noise
	atrue = x\b #Least-squares slope (no outliers)
	
	b[end]=-5.5
	acorrupt = x\b
end

# ╔═╡ 5a281581-f06e-47b0-9e60-f29b172ffc4f
al1 = Variable()

# ╔═╡ 849dc03f-61c0-4e88-bd15-3a760ef3539b
problem = minimize(norm(al1*x-b,1))

# ╔═╡ bc84ef36-03f4-4a22-bdfb-9592469a4a6a
solve!(problem,SCS.Optimizer(verbose=0))

# ╔═╡ 8690d8b7-9853-428e-a20d-f89937b254d2
xgrid = -2:0.01:2

# ╔═╡ 230cd14b-d536-4d95-afb6-35a14dad262c
begin
	scatter(x[1:end-1],b[1:end-1],legend=false)
	scatter!([x[end],b[end]])
	plot!(xgrid,atrue * xgrid,legend=false)
	plot!(xgrid,acorrupt * xgrid,legend=false)
	plot!(xgrid,al1.value * xgrid,legend=false)
end

# ╔═╡ Cell order:
# ╠═5a97aed2-9ff5-11eb-3a71-9fda2f44e8a6
# ╠═2560003b-00d8-48c5-851d-a0bc18a2a003
# ╠═5a281581-f06e-47b0-9e60-f29b172ffc4f
# ╠═849dc03f-61c0-4e88-bd15-3a760ef3539b
# ╠═bc84ef36-03f4-4a22-bdfb-9592469a4a6a
# ╠═8690d8b7-9853-428e-a20d-f89937b254d2
# ╠═230cd14b-d536-4d95-afb6-35a14dad262c
