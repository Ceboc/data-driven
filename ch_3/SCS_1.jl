### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 2f20a2ea-9c17-11eb-03e9-a92686f67b8b
using Images, FileIO, FFTW, PlutoUI

# ╔═╡ 7db294aa-f3d0-4652-8a56-b3cb18d59558
A = load("jelly.jpg")

# ╔═╡ 5ea4eb61-d697-4ff9-a76c-913f1e9ae59f
Ag = Gray.(A)

# ╔═╡ 3853636a-a5dc-4b85-9405-6ca8254ed5d5
At = fft(Float64.(Ag))

# ╔═╡ 70e0d853-db6d-4db7-8230-f25697814813
colorview(Gray,clamp01.(log10.(abs.(fftshift(At)))./maximum(log10.(abs.(At)))))

# ╔═╡ 12428881-eac7-47cc-9a25-9856253a3dbd
@bind keep NumberField(0.002:0.002:0.1, default=0.1)

# ╔═╡ e0b5bf20-a6b4-4d9e-afd7-7ba1377f74b3
begin
	Bt = sort(abs.(At[:]))
	thresh = Bt[Int64(floor((1-keep)*length(Bt)))]
	ind = abs.(At) .> thresh
	Atlow = At .* ind
end

# ╔═╡ f7821923-2aaf-4693-a043-b34c810aae99
Alow = ifft(ifftshift(Atlow));

# ╔═╡ dd4e039b-72e5-4262-84b9-54b001125027
colorview(Gray,clamp01.(log.(abs.(fftshift(Atlow)))./maximum(log.(abs.(Atlow)))))

# ╔═╡ faa82ee8-93f7-42ca-846c-37c01e3590ac
colorview(Gray,clamp01.(real.(Alow)))

# ╔═╡ Cell order:
# ╠═2f20a2ea-9c17-11eb-03e9-a92686f67b8b
# ╠═7db294aa-f3d0-4652-8a56-b3cb18d59558
# ╠═5ea4eb61-d697-4ff9-a76c-913f1e9ae59f
# ╠═3853636a-a5dc-4b85-9405-6ca8254ed5d5
# ╠═70e0d853-db6d-4db7-8230-f25697814813
# ╠═e0b5bf20-a6b4-4d9e-afd7-7ba1377f74b3
# ╠═f7821923-2aaf-4693-a043-b34c810aae99
# ╠═dd4e039b-72e5-4262-84b9-54b001125027
# ╠═12428881-eac7-47cc-9a25-9856253a3dbd
# ╠═faa82ee8-93f7-42ca-846c-37c01e3590ac
