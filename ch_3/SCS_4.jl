### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 9c55588c-a0be-11eb-30bb-57789dfa924a
using LinearAlgebra, DataFrames, Lasso, LassoPlot

# ╔═╡ 5d1e944a-daf8-44be-a1fd-03bcf592fff2
begin
	A = randn(100,10) # Matrix of possible predictors
	x = [0,0,1,0,0,0,-1,0,0,0] # 2 non zero predictors
	b = A*x + 2*randn(100,1)
end

# ╔═╡ a8875f94-9564-4cdf-aa17-166d4b9162e2
begin
	xL2 = pinv(A)*b
end

# ╔═╡ d47aa2af-f6c7-4553-8711-a78489eba336
m1 = fit(LassoPath,A,b[:])

# ╔═╡ 243bcf0e-9aa3-4cda-a529-8b8dca2e4ff5
plot(m1)

# ╔═╡ 2f3e46d4-b4f7-402a-ac02-a7ea49cbe3f2
selectmodel(m1, MinCVmse(m1, 10))

# ╔═╡ 28212b8f-54b8-4d48-ae4a-aced98bd149c
plot

# ╔═╡ Cell order:
# ╠═9c55588c-a0be-11eb-30bb-57789dfa924a
# ╠═5d1e944a-daf8-44be-a1fd-03bcf592fff2
# ╠═a8875f94-9564-4cdf-aa17-166d4b9162e2
# ╠═d47aa2af-f6c7-4553-8711-a78489eba336
# ╠═243bcf0e-9aa3-4cda-a529-8b8dca2e4ff5
# ╠═2f3e46d4-b4f7-402a-ac02-a7ea49cbe3f2
# ╠═28212b8f-54b8-4d48-ae4a-aced98bd149c
